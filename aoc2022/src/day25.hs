import Data.Char (digitToInt, intToDigit)

decode :: String -> Int
decode snafu = sum [conv n * p | (n, p) <- zip (reverse snafu) (iterate (*5) 1)]
    where conv x
            | x == '=' = -2
            | x == '-' = -1
            | otherwise = digitToInt x

base5 x
    | q == 0 = [r]
    | otherwise = r: base5 q
    where 
        q = x `div` 5
        r = x `mod` 5

encode 0 [] = []
encode r [] = [intToDigit r]
encode r (b5:b5s)
    | newB5 == 5 = encode 1 b5s ++ ['0']
    | newB5 == 4 = encode 1 b5s ++ ['-']
    | newB5 == 3 = encode 1 b5s ++ ['=']
    | otherwise = encode 0 b5s ++ [intToDigit newB5]
    where newB5 = b5 + r
        
main = do
    input <- lines <$> readFile "data/day25.txt"
    let fuel = sum [decode l | l <- input]
    let b5 = base5 fuel
    let snafu = encode 0 b5
    print snafu