-- X = rock, Y = paper, Z = scissors
scoreRps :: String -> Int
scoreRps choices
    | choices == "A X" = 1 + 3
    | choices == "A Y" = 2 + 6
    | choices == "A Z" = 3 + 0
    | choices == "B X" = 1 + 0
    | choices == "B Y" = 2 + 3
    | choices == "B Z" = 3 + 6
    | choices == "C X" = 1 + 6
    | choices == "C Y" = 2 + 0
    | choices == "C Z" = 3 + 3
    | otherwise = 0

-- X = lose, Y = tie, Z = win
scoreWinLoss :: String -> Int
scoreWinLoss choices
    | choices == "A X" = 3 + 0 
    | choices == "A Y" = 1 + 3 
    | choices == "A Z" = 2 + 6 
    | choices == "B X" = 1 + 0 
    | choices == "B Y" = 2 + 3 
    | choices == "B Z" = 3 + 6 
    | choices == "C X" = 2 + 0 
    | choices == "C Y" = 3 + 3 
    | choices == "C Z" = 1 + 6 
    | otherwise = 0

sumScores :: String -> (String -> Int) -> Int
sumScores choices score = sum (map score (lines choices))

main :: IO ()
main = do  
    choices <- readFile "data/day2.txt"
    print (sumScores choices scoreRps)
    print (sumScores choices scoreWinLoss)