import Debug.Trace

type Chamber = [[Bool]]
type Rock = [[Bool]]

rocks = [
    [[True, True, True, True]], -- --
    [[False, True, False], [True, True, True], [False, True, False]], -- +
    [[True, True, True], [False, False, True], [False, False, True]], -- _|
    [[True], [True], [True], [True]], -- |
    [[True, True], [True, True]] -- o
    ]

data Gamestate = Gamestate { chamber :: Chamber, rockId :: Int, x :: Int, y :: Int }

collide game
    | rLeft < 0 || rRight > 6 || rBot < 0 = True
    | rBot >= cHeight = False
    | otherwise = overlap
    where
        cHeight = length (chamber game)
        rock = rocks !! rockId game
        rHeight = length rock
        rWidth = length (head rock)
        rLeft = x game
        rRight = rLeft + rWidth - 1
        rBot = y game
        rTop = rBot + rHeight - 1
        rOverTop = min 0 (rTop - cHeight - 1)
        hSliceRock = drop rOverTop rock
        hSliceCh = take (length hSliceRock) (drop rBot (chamber game))
        vSliceCh row = take rWidth (drop rLeft row)
        overlap = or $ concat [[r && c | (r, c) <- zip rRow (vSliceCh cRow)] | (rRow, cRow) <- zip hSliceRock hSliceCh]

addRockToChamber game = [[chamberVal x y || rockVal x y | x <- [0..6]] | y <- [0..totalHeight - 1]]
    where
        rock = rocks !! rockId game
        rBot = y game
        rTop = rBot + length rock - 1
        rLeft = x game
        rRight = rLeft + length (head rock) - 1
        totalHeight = max (length (chamber game)) (rTop + 1)
        chamberVal xi yi
            | yi < length (chamber game) = chamber game !! yi !! xi
            | otherwise = False
        rockVal xi yi
            | yi >= rBot && yi <= rTop && xi >= rLeft && xi <= rRight = rock !! (yi - y game) !! (xi - x game)
            | otherwise = False

runSimulation :: String -> Gamestate -> Int -> Int -> Int -> Int
runSimulation windPattern game r t h
    | r == 0 = chamberHeight
    | chamberHeight > 200 = 100 + runSimulation windPattern truncateGame r t (h + 100)
    -- | hitBottom =  checkLoop $ checkLoop2 $ checkLoop3 $ runSimulation windPattern nextRock (r - 1) (t + 1) h
    | hitBottom = runSimulation windPattern nextRock (r - 1) (t + 1) h
    | otherwise = runSimulation windPattern nextY r (t + 1) h
    where
        chamberHeight = length (chamber game)
        truncateGame = game { chamber = drop 100 (chamber game), y = y game - 100 }
        wind = if cycle windPattern !! t == '>' then 1 else -1
        moveX = game { x = x game + wind}
        nextX = if collide moveX then game else moveX
        moveY = nextX { y = y nextX - 1}
        hitBottom = collide moveY
        nextY = if hitBottom then nextX else moveY
        nextChamber = addRockToChamber nextY
        nextRockId = cycle [0..4] !! (rockId nextY + 1)
        nextRock = Gamestate { chamber = nextChamber, rockId = nextRockId, x = 2, y = length nextChamber + 3}
        pat = [False, True, True, True, True, True, False]
        checkLoop a = if length (chamber nextRock) > 0 && last (chamber nextRock) == pat then trace (show (length (chamber nextRock) + h, r)) a else a
        checkLoop2 a = if length (chamber nextRock) > 1 && last (init (chamber nextRock)) == pat then trace (show (length (chamber nextRock) + h - 1, r)) a else a
        checkLoop3 a = if length (chamber nextRock) > 2 && last (init (init (chamber nextRock))) == pat then trace (show (length (chamber nextRock) + h - 2, r)) a else a
        traceChamber = trace (show $ map (map boolToChars) (take 100 (chamber game)))
        lookForStart a = if rockId game == 0 && t `mod` length windPattern == 0 then trace (show (length (chamber game) + h, r)) a else a

startSimulation windPattern rocks = runSimulation windPattern Gamestate { chamber = [], rockId = 0, x = 2, y = 3 } rocks 0 0

boolToChars b = if b then '#' else '.'

printChamber chamber = mapM_ print converted
    where converted = reverse (zip (map (map boolToChars) chamber) [0..])

main = do 
    windPattern <- readFile "data/day17.txt"
    let results = startSimulation windPattern 3600
    print results