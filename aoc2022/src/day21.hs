import Data.List (find)
import Data.Maybe (Maybe, isNothing, isJust, fromJust)
import Text.Read (readMaybe)
import qualified Data.Map as M


-- Part 1

parse :: String -> M.Map String String
parse input = M.fromList [(take 4 l, drop 6 l) | l <- lines input]

monkeyOps :: String -> (String, Char, String)
monkeyOps monkey = (take 4 monkey, monkey !! 5, drop 7 monkey)

solve :: M.Map String String -> String -> Int
solve monkeys key
    | isJust checkInt = fromIntegral (fromJust checkInt)
    | op == '+' = solve monkeys left + solve monkeys right
    | op == '-' = solve monkeys left - solve monkeys right
    | op == '*' = solve monkeys left * solve monkeys right
    | op == '/' = solve monkeys left `div` solve monkeys right
    where 
        monkey = fromJust (M.lookup key monkeys)
        checkInt = readMaybe monkey :: Maybe Int
        (left, op, right) = monkeyOps monkey


-- Part 2

-- Finds the next monkey up the chain that will use our value
findNextMonkey :: M.Map String String -> String -> (String, String)
findNextMonkey monkeys valKey = fromJust (find findValKey assocList)
    where 
        assocList = M.assocs monkeys
        findValKey (k, v) = left == valKey || right == valKey
            where 
                checkInt = readMaybe v :: Maybe Int
                (left, op, right) = if isNothing checkInt then monkeyOps v else ("", ' ', "")

changeJobs :: M.Map String String -> String -> M.Map String String -> M.Map String String
changeJobs monkeys key newMonkeys
    | nextKey == "root" = M.insert key (show (solve monkeys rootRight)) newMonkeys
    | otherwise = changeJobs monkeys nextKey (M.insert key newMonkeyOp newMonkeys)
    where 
        (nextKey, nextOps) = findNextMonkey monkeys key
        (left, op, right) = monkeyOps nextOps
        newMonkeyOp
            | op == '+' && key == left   = nextKey ++ " - " ++ right
            | op == '+' && key == right  = nextKey ++ " - " ++ left
            | op == '-' && key == left   = nextKey ++ " + " ++ right
            | op == '-' && key == right  =    left ++ " - " ++ nextKey
            | op == '*' && key == left   = nextKey ++ " / " ++ right
            | op == '*' && key == right  = nextKey ++ " / " ++ left
            | op == '/' && key == left   = nextKey ++ " * " ++ right
            | op == '/' && key == right  =    left ++ " / " ++ nextKey
        (_, _, rootRight) = monkeyOps (fromJust (M.lookup "root" monkeys))


main = do 
    monkeys <- parse <$> readFile "data/day21.txt"
    let root = solve monkeys "root"
    print root
    let monkeys2 = changeJobs monkeys "humn" monkeys
    let humn = solve monkeys2 "humn"
    print humn