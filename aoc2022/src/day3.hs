import Data.List(elemIndex)
import Data.Maybe(mapMaybe)
import Data.Char(ord)

itemPriority i = if ord i > 96 then ord i - 96 else ord i - 38
sharedItems (items1, items2) = map (items2 !!) (mapMaybe (`elemIndex` items2) items1)
compartments items = splitAt (length items `div` 2) items
sumPriorities rucksacks = sum $ map (itemPriority . head . sharedItems . compartments) rucksacks

groupBadge [items1, items2, items3] = head $ sharedItems (sharedItems (items1, items2), items3)

groups [] = []
groups rucksacks = take 3 rucksacks : groups (drop 3 rucksacks)

sumGroupBadges rucksacks = sum $ map (itemPriority . groupBadge) (groups rucksacks)

main = do  
        pile <- readFile "data/day3.txt"
        print (sumPriorities (lines pile))
        print (sumGroupBadges (lines pile))