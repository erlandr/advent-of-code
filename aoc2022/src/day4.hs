isContained (x1, x2, y1, y2)
    | x1 >= y1 && x2 <= y2 = 1
    | y1 >= x1 && y2 <= x2 = 1
    | otherwise = 0

doesOverlap (x1, x2, y1, y2)
    | x1 <= y2 && x2 >= y1 = 1
    | y1 <= x2 && y2 >= x1 = 1
    | otherwise = 0

parse :: String -> [(Int, Int, Int, Int)]
parse x = map read (lines x)

main = do  
        pile <- readFile "data/day4.txt"
        print (sum $ map isContained (parse pile))
        print (sum $ map doesOverlap (parse pile))