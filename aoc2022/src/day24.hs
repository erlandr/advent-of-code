import Data.Array
import Debug.Trace

makeMaps input = (elves, (nWind, sWind, eWind, wWind))
    where 
        valley = [init (drop 1 row) | row <- init (drop 1 (lines input))]
        (width, height) = (length (head valley), length valley)
        nWind = array ((0,0), (height-1,width-1)) (concat [[((iy,ix), wind == '^') | (ix, wind) <- zip [0..] row] | (iy, row) <- zip [0..] valley])
        sWind = array ((0,0), (height-1,width-1)) (concat [[((iy,ix), wind == 'v') | (ix, wind) <- zip [0..] row] | (iy, row) <- zip [0..] valley])
        eWind = array ((0,0), (height-1,width-1)) (concat [[((iy,ix), wind == '>') | (ix, wind) <- zip [0..] row] | (iy, row) <- zip [0..] valley])
        wWind = array ((0,0), (height-1,width-1)) (concat [[((iy,ix), wind == '<') | (ix, wind) <- zip [0..] row] | (iy, row) <- zip [0..] valley])
        elves = array ((0,0), (height-1,width-1)) (concat [[((iy,ix), False) | (ix, wind) <- zip [0..] row] | (iy, row) <- zip [0..] valley])

isBliz windMap turn x y = nBliz || sBliz || eBliz || wBliz
        where 
            (nWind, sWind, eWind, wWind) = windMap
            ((_, _), (height, width)) = bounds nWind
            nBliz = nWind ! ((y + turn) `mod` (height+1), x)
            sBliz = sWind ! ((y - turn) `mod` (height+1), x)
            eBliz = eWind ! (y, (x - turn) `mod` (width+1))
            wBliz = wWind ! (y, (x + turn) `mod` (width+1))

propose windMap elves (startY, startX) turn = array ((0,0), (height,width)) (concat [[((y,x), elfInSpace x y) | x <- [0..width]] | y <- [0..height]])
    where
        ((_, _), (height, width)) = bounds elves
        elfInSpace x y
            | isBliz windMap turn x y           = False     -- No elves in blizzard
            | x == startX && y == startY        = True      -- Elves start  from top left (or bot right)
            | elves ! (y, x)                    = True      -- Elves here?
            | x > 0 && elves ! (y, x-1)         = True      -- Elves left?
            | x < width && elves ! (y, x+1)     = True      -- Elves right?
            | y > 0 && elves ! (y-1, x)         = True      -- Elves up?
            | y < height && elves ! (y+1, x)    = True      -- Elves down?
            | otherwise                         = False

simulate windMap elves start goal turn
    | reachedEnd = turn + 1
    | otherwise = simulate windMap nextElves start goal (turn + 1)
    where
        ((_, _), (height, width)) = bounds elves
        nextElves = propose windMap elves start turn
        reachedEnd = nextElves ! goal || turn == 2000

main = do
    (elves, windMap) <- makeMaps <$> readFile "data/day24.txt"
    let ((_, _), (height, width)) = bounds elves
    let goal = (height, width)
    let firstTrip = simulate windMap elves (0,0) goal 1
    print firstTrip
    let forgotSnacks = simulate windMap elves goal (0,0) (firstTrip + 1)
    print forgotSnacks
    let secondTrip = simulate windMap elves (0,0) goal (forgotSnacks + 1)
    print secondTrip