import Data.List.Split (splitOn)
import Data.List (nub)

data Rope = Rope { segments :: [(Int, Int)], visited :: [(Int, Int)] } deriving (Show)
makeRope numSegments = Rope { segments = replicate numSegments (0, 0), visited = [(0, 0)]}

move 'U' (x, y) = (x, y + 1)
move 'D' (x, y) = (x, y - 1)
move 'L' (x, y) = (x - 1, y)
move 'R' (x, y) = (x + 1, y)

delta (x1, y1) (x2, y2) = (x1 - x2, y1 - y2)

follow (x1, y1) (x2, y2) 
    | abs deltaX == 2 || abs deltaY == 2 = (x2 + signum deltaX, y2 + signum deltaY)
    | otherwise = (x2, y2)
    where (deltaX, deltaY) = delta (x1, y1) (x2, y2)

followChain moved [] = moved
followChain moved remaining = followChain (moved ++ [follow (last moved) (head remaining)]) (tail remaining)

record rope = rope { visited = last (segments rope) : visited rope}

moveRope dir rope = rope { segments = followChain [newHead] (tail (segments rope)) }
    where newHead = move dir (head (segments rope))

moveRopeSpaces dir rope 0 = rope
moveRopeSpaces dir rope n = moveRopeSpaces dir (record (moveRope dir rope)) (n-1)

simulate rope [] = rope
simulate rope commands = simulate (moveRopeSpaces dir rope n) (tail commands)
    where (dir, n) = head commands

parse input = map ((\[dir,n] -> (head dir, read n)) . splitOn " ") (lines input)

main = do
    commands <- parse <$> readFile "data/day9.txt"
    let rope = makeRope 2
    let finalRope = simulate rope commands
    print (length (nub (visited finalRope)))
    let rope2 = makeRope 10
    let finalRope2 = simulate rope2 commands
    print (length (nub (visited finalRope2)))