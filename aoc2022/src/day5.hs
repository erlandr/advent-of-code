import Data.String.Utils(strip)
import Data.List.Utils(replace)
import Data.List.Split(splitOn)

stacks = ["VNFSMPHJ", "QDJMLRS", "BWSCHDQN", "LCSR", "BFPTVM", "CNQRT", "RVG", "RLDPSZC", "FBPGVJSD"]

parse :: String -> [(Int, Int, Int)]
parse x = map read (lines x)

moveCrateTo stacks (n, from, to) = take n (stacks !! (from - 1)) ++ stacks !! (to - 1)
moveCrateFrom stacks (n, from, to) = drop n (stacks !! (from - 1))

getStack stacks (n, from, to) x
    | x == to = moveCrateTo stacks (n, from, to)
    | x == from = moveCrateFrom stacks (n, from, to)
    | otherwise = stacks !! (x - 1)

crateMover9000 :: [String] -> (Int, Int, Int) -> [String]
crateMover9000 stacks (0, from , to) = stacks
crateMover9000 stacks (n, from, to) = crateMover9000 (map (getStack stacks (1, from, to)) [1..9]) (n - 1, from , to)

crateMover9001 :: [String] -> (Int, Int, Int) -> [String]
crateMover9001 stacks (n, from, to) = map (getStack stacks (n, from, to)) [1..9]

doWork :: [String] -> ([String] -> (Int, Int, Int) -> [String]) -> [(Int, Int, Int)] -> [String]
doWork stacks crane commands
    | not (null commands) = doWork (crane stacks (head commands)) crane (drop 1 commands)
    | otherwise = stacks

main = do  
        contents <- readFile "data/day5.txt"
        print (map head (doWork stacks crateMover9000 (parse contents)))
        print (map head (doWork stacks crateMover9001 (parse contents)))