import Data.Char (isDigit)
import Data.List (sortBy, elemIndex)

removeBlank [] = []
removeBlank (x1:x2:_:xs) = x1 : x2 : removeBlank xs

parse input = removeBlank (lines input ++ [""])

getDigit [] = []
getDigit (x:xs)
    | isDigit x = x : getDigit xs
    | otherwise = []

bracketAfterNum (x:xs)
    | x == ',' || x == ']' = ']' : x : xs
    | otherwise = x : bracketAfterNum xs

leftFirst [] (r:rs) = True
leftFirst (l:ls) [] = False
leftFirst (l:ls) (r:rs)
    | (l:ls) == (r:rs) = True
    | l == r = leftFirst ls rs
    | l == '[' && r /= '[' = leftFirst (l:ls) ('[' : bracketAfterNum (r:rs))
    | l /= '[' && r == '[' = leftFirst ('[' : bracketAfterNum (l:ls)) (r:rs)
    | l == ']' && r /= ']' = True
    | l /= ']' && r == ']' = False
    | isDigit l && not (isDigit r) = False
    | not (isDigit l) && isDigit r = True
    | isDigit l && isDigit r = (read dl :: Int) < (read dr :: Int)
    | otherwise = error ((l:ls) ++ " - " ++ (r:rs))
    where dl = getDigit (l:ls)
          dr = getDigit (r:rs)

-- Part 1
checkOrder [] = []
checkOrder (left:right:xs) = leftFirst left right : checkOrder xs

sumOfI inOrder = sum [if x then i else 0 | (x,i) <- zip inOrder [1..]]

-- Part 2
sortLeftFirst left right = if leftFirst left right then LT else GT

decoderKey div1 div2 packets = (index1 + 1) * (index2 + 1)
    where sorted = sortBy sortLeftFirst (div1 : div2 : packets)
          (Just index1) = elemIndex div1 sorted
          (Just index2) = elemIndex div2 sorted 


main = do
    packets <- parse <$> readFile "data/day13.txt"
    let inOrder = checkOrder packets
    print (sumOfI inOrder)
    let div1 = "[[2]]"
    let div2 = "[[6]]"
    let decoded = decoderKey div1 div2 packets
    print decoded