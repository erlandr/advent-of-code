import Data.List (find, nub)
import Data.List.Split (splitOn, chunksOf)
import Data.Maybe (fromJust)
import Control.Parallel

type VKey = String
type VFlow = Int
type VDist = Int
type Valve = (VFlow, VKey, [VKey])
type FlowValve = (VKey, VFlow, [(VKey, VDist)])

parseLine :: String -> Valve
parseLine l = (flow, key, tunnels)
    where key = drop 6 (take 8 l)
          flow = read (last (splitOn "=" (head  (splitOn "; " l))))
          tunnels 
            | ',' `elem` l = splitOn ", " (drop 23 (last (splitOn "; " l)))
            | otherwise = [last (splitOn " " l)]

parse :: String -> [Valve]
parse input = [parseLine l | l <- lines input]

makeFlowValve :: Valve -> [Valve] -> [Valve] -> FlowValve
makeFlowValve valve allValves usefulValves = (name, flow, flowValveDistances)
    where 
        (flow, name, _) = valve        
        getValve vName = fromJust (find (\(_, n, _) -> n == vName) allValves)
        flowValveDistances = filter (\(n, _) -> getValve n `elem` usefulValves) allDistances
        allDistances = nub $ concat [[(v, distance) | v <- valves] | (valves, distance) <- zip (mapDistances [[name]]) [0..]]
        mapDistances found
            | null nextHop = found
            | otherwise = mapDistances (found ++ [nextHop])
            where
                theseValves = [getValve n | n <- last found]
                previouslyFound = concat found
                nextHop = filter (`notElem` previouslyFound) (concat [nb | (_, _, nb) <- theseValves])

findFv name = find (\(n, _, _) -> n == name)

score :: [FlowValve] -> Int -> [String] -> FlowValve -> Int -> Int -> Int -> Int
score flowValves currentFlow visited flowValve travelTime openTime timer
    | timer == 0 = 0
    | travelTime > 0 = currentFlow + score flowValves currentFlow visited flowValve (travelTime - 1) openTime (timer - 1)
    | openTime > 0 = currentFlow + score flowValves (currentFlow + flow) (name : visited) flowValve travelTime (openTime - 1) (timer - 1)
    | otherwise = currentFlow + maximum branches
    where
        (name, flow, distances) = flowValve
        leftToVisit = filter (\(n, _) -> n `notElem` visited) distances
        visits
            | null leftToVisit = [(("AA", 0, []), 30)]
            | otherwise = [(fromJust (findFv nextName flowValves), nextDist) | (nextName, nextDist) <- leftToVisit ]
        branches = [score flowValves currentFlow visited nextFv (nextDist - 1) 1 (timer - 1) | (nextFv, nextDist) <- visits]

runScore flowValves = score flowValves 0 ["AA"] (head flowValves) 0 0

runTeamScore (fv1, fv2) = par s1 par s2 (s1 + s2)
    where 
        s1 = runScore fv1 26
        s2 = runScore fv2 26

splitWork bits allValves
    | bits == replicate lenFv 1 = []
    | sum bits == half = (tasks1, tasks2) : splitWork (increment bits) allValves
    | otherwise = splitWork (increment bits) allValves
    where
        usefulValves = filter (\(flow, _, _) -> flow > 0) allValves
        valves1 = [v | (v, m) <- zip usefulValves bits, m == 1]
        valves2 = [v | (v, m) <- zip usefulValves bits, m == 0]
        lenFv = length usefulValves
        half = lenFv `div` 2 + lenFv `mod` 2
        start1 = makeFlowValve (0, "AA", []) allValves valves1
        start2 = makeFlowValve (0, "AA", []) allValves valves2
        tasks1 = start1 : [makeFlowValve (flow, name, tunnels) allValves valves1 | (flow, name, tunnels) <- valves1]
        tasks2 = start2 : [makeFlowValve (flow, name, tunnels) allValves valves2 | (flow, name, tunnels) <- valves2]
        increment [] = []
        increment (b:bits)
            | b == 0 = 1 : bits
            | otherwise = 0 : increment bits

runPermutations workloads = par r1 (par r2 (par r3 (par r4 (par r5 (par r6))))) (maximum [r1, r2, r3, r4, r5, r6])
    where 
        (c1:c2:c3:c4:c5:c6) = chunksOf (length workloads `div` 6 + 1) workloads
        r1 = maximum [runTeamScore wl | wl <- c1]
        r2 = maximum [runTeamScore wl | wl <- c2]
        r3 = maximum [runTeamScore wl | wl <- c3]
        r4 = maximum [runTeamScore wl | wl <- c4]
        r5 = maximum [runTeamScore wl | wl <- c5]
        r6 = maximum [runTeamScore wl | wl <- head c6]

main = do
    allValves <- parse <$> readFile "data/day16.txt"
    let usefulValves = filter (\(f,_,_) -> f > 0) allValves
    let start = head [makeFlowValve (flow, name, tunnels) allValves usefulValves | (flow, name, tunnels) <- allValves, name == "AA"]
    let flowValves = start : [makeFlowValve (flow, name, tunnels) allValves usefulValves | (flow, name, tunnels) <- allValves, flow > 0]
    let s = runScore flowValves 30
    print s
    let workloads = splitWork (replicate (length usefulValves) 0) allValves
    let allRuns = runPermutations workloads
    let s2 = allRuns
    print s2