import Data.List (nub, sort, find)
import Data.Maybe (fromJust)

type Inum = (Int, Int)

parse :: String -> [Inum]
parse input = [(i, n) | (i, n) <- zip [0..] (map read (lines input))]

mix (startI, n) iList
    | newI > currentI = [if xi < currentI || xi > newI then (xi, xn) else if xi == currentI then (newI, n) else (xi - 1, xn) | (xi, xn) <- iList]
    | newI < currentI = [if xi > currentI || xi < newI then (xi, xn) else if xi == currentI then (newI, n) else (xi + 1, xn) | (xi, xn) <- iList]
    | otherwise = iList
    where
        (currentI, _) = iList !! startI 
        maxI = length iList - 1
        iShift = (currentI + n) `mod` maxI
        newI = if currentI /= 0 && iShift == 0 then maxI else iShift

mixList iList [] = sort iList
mixList iList (x:xs) = mixList (mix x iList) xs

decrypt iNums = [(i, n * 811589153) | (i, n) <- iNums]

sumCoords mixed = one + two + three
    where 
        (zeroI, _) = fromJust $ find (\(i,n) -> n == 0) mixed
        (_, one) = mixed !! ((zeroI + 1000) `mod` length mixed)
        (_, two) = mixed !! ((zeroI + 2000) `mod` length mixed)
        (_, three) = mixed !! ((zeroI + 3000) `mod` length mixed)

main = do 
    input <- parse <$> readFile "data/day20.txt"
    let mixed = mixList input input
    let part1 = sumCoords mixed
    print part1
    let decryptedInput = decrypt input
    let x10 = concat (replicate 10 decryptedInput)
    let mixed2 = mixList decryptedInput x10
    let part2 = sumCoords mixed2
    print part2