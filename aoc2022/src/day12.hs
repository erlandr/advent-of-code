import Data.Char (ord)

actualElev x
    | x == 'S' = 'a'
    | x == 'E' = 'z'
    | otherwise = x

canWalk dest origin = ord (actualElev dest) - ord (actualElev origin) <= 1

data Node = Node { elevation :: Char, neighbors :: [(Int, Int)], distance :: Int } deriving (Show)

makeGraph input start = [[makeNode x y item | (x, item) <- zip [0..] row] | (y, row) <- zip [0..] input]
  where width = length (head input)
        height = length input
        makeNode x y item = Node { elevation = item, neighbors = up ++ down ++ left ++ right, distance = if item == start then 0 else 9999 }
          where up = [(x, y-1) | y > 0 && canWalk ((input !! (y-1)) !! x) item]
                down = [(x, y+1) | y < height - 1 && canWalk ((input !! (y+1)) !! x) item]
                left = [(x-1, y) | x > 0 && canWalk ((input !! y) !! (x-1)) item]
                right = [(x+1, y) | x < width - 1 && canWalk ((input !! y) !! (x+1)) item]

closest graph [] = 9999
closest graph neighbors = minimum [distance node | node <- [graph !! y !! x | (x, y) <- neighbors]] + 1

makeDistance graph = [[node { distance = min (distance node) (closest graph (neighbors node)) } | node <- row] | row <- graph]

walk graph (endX, endY)
    | distance (graph !! endY !! endX) < 9999 = graph
    | otherwise = walk (makeDistance graph) (endX, endY)

main = do
    input <- lines <$> readFile "data/day12.txt"
    let graph = makeGraph input 'E'
    let walkedGraph = walk graph (0,20)
    let part1 = distance (head (walkedGraph !! 20))
    let part2 = minimum $ map (minimum . map distance . filter (\n -> elevation n == 'a')) walkedGraph
    print (part1, part2)