import Data.List.Split (splitOn)
import Data.List (sort)


monkeySee :: String -> (Int -> Int)
monkeySee opString item
    | op == "+" = item + read val
    | val == "old" = item * item
    | op == "*" = item * read val
    where opParts = splitOn " " (drop 10 opString)
          (op, val) = (opParts !! 4, last opParts)

monkeyDo :: [String] -> (Int, Int, Int)
monkeyDo testStrings = (divisor, monkTrue, monkFalse)
    where divisor = read (last (splitOn " " (head testStrings)))
          monkTrue = read (last (splitOn " " (testStrings !! 1)))
          monkFalse = read (last (splitOn " " (testStrings !! 2)))

data Monkey = Monkey { items :: [Int], see :: Int -> Int, divWorry :: Int, divTest :: Int, monkTrue :: Int, monkFalse :: Int, inspectionCount :: Int}

makeMonkey :: [String] -> Int -> Monkey
makeMonkey inputs divWorry = Monkey { items = startItems, see = operation, divWorry = divWorry, divTest = divTest, monkTrue = monkTrue, monkFalse = monkFalse , inspectionCount = 0 }
    where startItems = map read (splitOn ", " (last (splitOn ": " (inputs !! 1))))
          operation = monkeySee (inputs !! 2)
          (divTest, monkTrue, monkFalse) = monkeyDo (drop 3 inputs)

parse :: [String] -> Int -> [Monkey]
parse input divWorry = [makeMonkey (take 6 (drop (7 * i) input)) divWorry | i <- [0..7]]


removeItem :: Monkey -> (Int, Monkey)
removeItem monkey = (item, monkey {items = drop 1 (items monkey), inspectionCount = inspectionCount monkey + 1})
    where item = if not (null (items monkey)) then head (items monkey) else (-1)
    
addItem :: Monkey -> Int -> Monkey
addItem monkey item = monkey {items = items monkey ++ [item]}

update :: [Monkey] -> Monkey -> Int -> [Monkey]
update monkies monkey index = [ if i == index then monkey else m | (m, i) <- zip monkies [0..] ]

throwItems :: [Monkey] -> Int -> [Monkey]
throwItems monkies i
    | item /= -1 = throwItems (update newMonkies (addItem (monkies !! throwMonkeyIndex) newItem) throwMonkeyIndex) i
    | otherwise = monkies
    where (item, monkey) = removeItem (monkies !! i)
          newMonkies = update monkies monkey i
          divTestProduct = product (map divTest monkies)
          newItem = (see monkey item `div` divWorry monkey) `mod` divTestProduct
          throwMonkeyIndex = if newItem `mod` divTest monkey == 0 then monkTrue monkey else monkFalse monkey

monkeyRound :: Int -> [Monkey] -> [Monkey]
monkeyRound 8 monkies = monkies
monkeyRound i monkies = monkeyRound (i+1) (throwItems monkies i)

playRounds :: Int -> [Monkey] -> [Int]
playRounds 0 monkies = map inspectionCount monkies
playRounds i monkies = playRounds (i-1) (monkeyRound 0 monkies)

monkeyBusiness :: [Int] -> Int
monkeyBusiness results = head sortedResults * sortedResults !! 1
    where sortedResults = reverse (sort results)


main = do
    input <- lines <$> readFile "data/day11.txt"
    let monkies = parse input 3
    let results = playRounds 20 monkies
    print (monkeyBusiness results)
    let monkies2 = parse input 1
    let results2 = playRounds 10000 monkies2
    print (monkeyBusiness results2)