import Data.String.Utils(strip)
import Data.List

parse x n i = if (\x -> nub x == x) (take n x) then i + n else parse (drop 1 x) n (i + 1)

main = do  
        contents <- readFile "data/day6.txt"
        print (parse (strip contents) 4 0)
        print (parse (strip contents) 14 0)