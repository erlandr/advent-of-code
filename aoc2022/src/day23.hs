import Data.List (transpose, find)
import Data.Maybe (isNothing)

elf = '#'
empty = '.'

countElves field = sum [length (filter (/= empty) row) | row <- field]

rowGen field = zip3 (newRow : field) field (tail field ++ [newRow])
    where 
        width = length (head field)
        newRow = replicate width empty

colGen (rowN, row, rowS) = zip3 (transpose [empty : rowN, empty : row, empty : rowS]) (transpose [rowN, row, rowS]) (transpose [tail rowN ++ [empty], tail row ++ [empty], tail rowS ++ [empty]])

isElf (west, mid, east) = mid !! 1 == elf

propose roundNum field = [[if isElf (west, mid, east) then moveDir (west, mid, east) else empty | (west, mid, east) <- colGen (rowN, row, rowS)] | (rowN, row, rowS) <- rowGen field]
    where        
        openDir dir = head dir /= elf && dir !! 1 /= elf && dir !! 2 /= elf
        openNorth (west, mid, east) = if openDir [head west, head mid, head east] then '^' else elf
        openSouth (west, mid, east) = if openDir [west !! 2, mid !! 2, east !! 2] then 'v' else elf
        openWest (west, mid, east) = if openDir west then '<' else elf
        openEast (west, mid, east) = if openDir east then '>' else elf
        openDirs (west, mid, east) = [openNorth (west, mid, east), openSouth (west, mid, east), openWest (west, mid, east), openEast (west, mid, east)]
        moveDir (west, mid, east)
            | od == "^v<>" = elf
            | head sd /= elf = head sd
            | sd !! 1 /= elf = sd !! 1
            | sd !! 2 /= elf = sd !! 2
            | sd !! 3 /= elf = sd !! 3
            | otherwise = elf
            where
                rnd = roundNum `mod` 4
                od = openDirs (west, mid, east)
                sd = drop rnd od ++ take rnd od

expand field = newRow : [ empty : row ++ [empty] | row <- field] ++ [newRow]
    where 
        width = length (head field) + 2
        newRow = replicate width empty

moveElves field = [[if isElf (west, mid, east) || numMoves (west, mid, east) == 1 then elf else mid !! 1 | (west, mid, east) <- colGen (rowN, row, rowS)] | (rowN, row, rowS) <- rowGen field]
    where
        numMoves (west, mid, east) = numNorth mid + numSouth mid + numWest west + numEast east
        numNorth mid = if head mid == 'v' then 1 else 0
        numSouth mid = if last mid == '^' then 1 else 0
        numWest west = if west !! 1 == '>' then 1 else 0
        numEast east = if east !! 1 == '<' then 1 else 0

cleanup field = [[checkMove (west, mid, east) | (west, mid, east) <- colGen (rowN, row, rowS)] | (rowN, row, rowS) <- rowGen field]
    where
        checkMove (west, mid, east)
            | c == '^' = if head mid == elf then empty else elf
            | c == 'v' = if last mid == elf then empty else elf
            | c == '<' = if west !! 1 == elf then empty else elf
            | c == '>' = if east !! 1 == elf then empty else elf
            | otherwise = c
            where 
                c = mid !! 1

trim :: [String] -> [String]
trim field = right
    where
        findElf = find (==elf)
        top = if isNothing (findElf (head field)) then tail field else field
        bot = if isNothing (findElf (last top)) then init top else top
        leftCol = [head row | row <- bot]
        left = if isNothing (findElf leftCol) then [tail row | row <- bot] else bot
        rightCol = [last row | row <- bot]
        right = if isNothing (findElf rightCol) then [init row | row <- left] else left

elfRound roundNum = trim . cleanup . moveElves . expand . propose roundNum

simulate 10 field = field
simulate roundNum field = simulate (roundNum + 1) (elfRound roundNum field)

countEmpty field = width * height - countElves field
    where
        width = length (head field)
        height = length field

compareRounds 1000 field = 999999999
compareRounds roundNum field = if newField == field then roundNum + 1 else compareRounds (roundNum + 1) newField
    where newField = elfRound roundNum field

main = do 
    field <- lines <$> readFile "data/day23.txt"
    mapM_ print field
    print (countElves field)
    let p = propose 0 field
    mapM_ print p
    print (countElves p)
    let e = expand p
    mapM_ print e
    print (countElves e)
    let m = moveElves e
    mapM_ print m
    print (countElves m)
    let c = cleanup m
    mapM_ print c
    print (countElves c)
    let end = trim (simulate 0 field)
    mapM_ print end
    print (countElves end)
    let numEmpty = countEmpty end
    print numEmpty
    let part2 = compareRounds 0 field
    print part2