import Data.List.Split (splitOn)

-- Chart
type Tile = Char
emptyTile = ' '
rockTile = '#'
sandTile = 'o'

type Scan = [[(Int, Int)]]
parse :: String -> Scan
parse input = map ( map ((\i -> (head i, last i)) . (map read . splitOn ",")) . splitOn " -> ") (lines input)

type Boundaries = (Int, Int, Int, Int)
findBoundaries :: Scan -> Boundaries
findBoundaries scan = (minimum xs, maximum xs, min 0 (minimum ys), maximum ys) 
    where scanPoints = concat scan
          xs = [x | (x,y) <- scanPoints]
          ys = [y | (x,y) <- scanPoints]

fillSegment :: (Int, Int) -> (Int, Int) -> [(Int, Int)]
fillSegment (x1, y1) (x2, y2) = concat [[(x,y) | x <- [minX..maxX]] | y <- [minY..maxY]]
    where minX = min x1 x2
          maxX = max x1 x2
          minY = min y1 y2
          maxY = max y1 y2

fillLine :: [(Int, Int)] -> [(Int, Int)]
fillLine rockSegments = concat [fillSegment r1 r2 | (r1, r2) <- zip rockSegments (tail rockSegments)]

fillRocks :: [[(Int, Int)]] -> [(Int, Int)]
fillRocks = concatMap fillLine

type Chart = [String]
makeChart :: Boundaries -> [(Int, Int)] -> Chart
makeChart (x1, x2, y1, y2) rocks = [[if (x,y) `elem` rocks then rockTile else emptyTile | x <- [x1..x2]] | y <- [y1..y2]]

addFloor :: Chart -> Chart
addFloor chart = init chart ++ [replicate width rockTile]
    where width = length (head chart)

updateRow x row = take x row ++ [sandTile] ++ drop (x + 1) row

updateChart :: [String] -> Sand -> Chart
updateChart chart (x, y) = [if iy == y then updateRow x row else row | (iy, row) <- zip [0..] chart]

-- Sand
type Sand = (Int, Int)
makeSand :: Int -> Sand
makeSand x = (x, 0)

updateSand :: Chart -> Boundaries -> Sand -> Sand
updateSand chart bounds (x, y)
    | y == boundB || x == il || x == ir  = (-1, -1)                                         -- hits boundary
    | (chart !! (y + 1) !! x) == emptyTile = updateSand chart bounds (x, y + 1)             -- drop down by 1
    | (chart !! (y + 1) !! (x - 1)) == emptyTile = updateSand chart bounds (x - 1, y + 1)   -- drop down and left by 1
    | (chart !! (y + 1) !! (x + 1)) == emptyTile = updateSand chart bounds (x + 1, y + 1)   -- drop down and right by 1
    | otherwise = (x, y)                                                                    -- settles here
    where (boundL, boundR, _, boundB) = bounds
          il = 0
          ir = boundR - boundL

-- Solve
part1 chart bounds
    | sand == (-1, -1) = 1
    | otherwise = 1 + part1 (updateChart chart sand) bounds
    where (boundL, boundR, _, boundB) = bounds
          sand = updateSand chart bounds (makeSand (500 - boundL))

part2 chart bounds
    | y == 0 = 1
    | otherwise = 1 + part2 (updateChart chart (x, y)) bounds
    where (boundL, boundR, boundT, boundB) = bounds
          sourceX = 500 - boundL
          (x, y) = updateSand chart bounds (makeSand (500 - boundL))


main = do
    scan <- parse <$> readFile "data/day14.txt"
    let bounds = findBoundaries scan
    let rocks = fillRocks scan
    let chart = makeChart bounds rocks
    let results1 = part1 chart bounds
    print results1

    let bounds2 = (329, 671, 0, 170)
    let chart2 = addFloor (makeChart bounds2 rocks) 
    let results2 = part2 chart2 bounds2
    print results2
