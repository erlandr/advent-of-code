data Choice = Choice { win :: Char, tie :: Char, lose :: Char, points :: Int}

rps :: Char -> Choice
rps 'R' = Choice { win = 'S', tie = 'R', lose = 'P', points = 1 }
rps 'P' = Choice { win = 'R', tie = 'P', lose = 'S', points = 2 }
rps 'S' = Choice { win = 'P', tie = 'S', lose = 'R', points = 3 }

decodeOpponent :: Char -> Char
decodeOpponent 'A' = 'R'
decodeOpponent 'B' = 'P'
decodeOpponent 'C' = 'S'

decodeYouRps :: Char -> Char -> Char
decodeYouRps _ 'X' = 'R'
decodeYouRps _ 'Y' = 'P'
decodeYouRps _ 'Z' = 'S'

decodeYouWinLoss :: Char -> Char -> Char
decodeYouWinLoss opponent 'X' = win (rps opponent)
decodeYouWinLoss opponent 'Y' = tie (rps opponent)
decodeYouWinLoss opponent 'Z' = lose (rps opponent)

score :: Char -> Char -> Int
score opponent you
    | win yourChoice == opponent = 6 + points yourChoice
    | tie yourChoice == opponent = 3 + points yourChoice
    | lose yourChoice == opponent = 0 + points yourChoice
    where yourChoice = rps you

scoreRps :: String -> Int
scoreRps encoded = score opponent you
    where opponent = decodeOpponent (head encoded)
          you = decodeYouRps opponent (last encoded)

scoreWinLoss :: String -> Int
scoreWinLoss encoded = score opponent you
    where opponent = decodeOpponent (head encoded)
          you = decodeYouWinLoss opponent (last encoded)

sumScores :: String -> (String -> Int) -> Int
sumScores encodedChoices scoreFunc = sum (map scoreFunc (lines encodedChoices))

main :: IO ()
main = do  
    encodedChoices <- readFile "data/day2.txt"
    print (sumScores encodedChoices scoreRps)
    print (sumScores encodedChoices scoreWinLoss)