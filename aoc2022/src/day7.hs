import Data.List.Split (splitOn)
import Data.List (sort)

tailOrEmpty [] = []
tailOrEmpty x = tail x
sumParentDir sizes = if length sizes > 1 then sum (take 2 sizes) : drop 2 sizes else []

run cmds sizes totals
    | null cmds && null sizes = totals
    | null cmds || take 7 c == "$ cd .." = run (tailOrEmpty cmds) (sumParentDir sizes) (head sizes : totals)
    | take 4 c == "$ cd" = run (tail cmds) (0 : sizes) totals
    | take 4 c == "$ ls" || take 3 c == "dir" = run (tail cmds) sizes totals
    | otherwise = run (tail cmds) (read (head (splitOn " " c)) + head sizes : tail sizes) totals
    where c = if not (null cmds) then head cmds else []

sumLess100k cmds = sum (filter (<100000) (run cmds [] []))
sizeToDelete totalSize = 30000000 - (70000000 - totalSize)
sizeOfDirToRm cmds = head (filter (> sizeToDelete (last totals)) totals)
    where totals = sort (run cmds [] [])

main = do
    inputs <- readFile "data/day7.txt"
    print (sumLess100k (lines inputs))
    print (sizeOfDirToRm (lines inputs))