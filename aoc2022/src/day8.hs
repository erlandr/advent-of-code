import Data.List (transpose)
import Data.ByteString (count)

-- Part 1
visible mask tallest [] = mask
visible mask tallest trees = visible ((head trees > tallest) : mask) (max (head trees) tallest) (tail trees)

combineVisibleLine = zipWith max
combineVisibleGrid = zipWith combineVisibleLine

survey trees = combineVisibleLine l r
    where l = reverse (visible [] ' ' trees)
          r = visible [] ' ' (reverse trees)

surveyGrid grid = combineVisibleGrid r c
    where r = map survey grid
          c = transpose (map survey (transpose grid))

sumVisibleTrees trees = length (filter (==True) trees)
sumVisibleGrid grid = sum (map sumVisibleTrees (surveyGrid grid))

-- Part 2
visibleFromTree height count [] = count
visibleFromTree height count trees = visibleFromTree height (count + 1) (if head trees < height then tail trees else [])

treeView left [] visMap = visMap
treeView left right visMap = treeView (head right : left) (tail right) (visMap ++ [visibleFromTree (head right) 0 left * visibleFromTree  (head right) 0 (tail right)])

treeViewGrid visMap [] = visMap
treeViewGrid visMap grid = treeViewGrid (visMap ++ [treeView [] (head grid) []]) (tail grid)

combineTreeView = zipWith (*)
combineTreeViewGrid visMap grid = zipWith combineTreeView r c
    where r = treeViewGrid [] grid
          c = transpose (treeViewGrid [] (transpose grid))
maxTreeView visMap = maximum (map maximum visMap)

main = do
    grid <- lines <$> readFile "data/day8.txt"
    print (sumVisibleGrid grid)
    print (maxTreeView $ combineTreeViewGrid [] grid)