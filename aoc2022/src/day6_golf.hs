import Data.List
f x n i|(\x->nub x==x)(take n x)=i+n|1<2=f(tail x)n(i+1)
main=do
 x<-readFile "data/day6.txt"
 print(f x 4 0)
 print(f x 14 0)