import Data.List.Split (splitOn)

parseCmd row 
    | take 4 row == "addx" = [read (last (splitOn " " row)), 0]
    | otherwise = [0]
parse input = concatMap parseCmd (lines input)

execute vals [] = vals
execute vals commands = execute (vals ++ [last vals + head commands]) (tail commands)

strength vals cycle = vals !! (cycle - 1) * cycle
sumStrength cycles vals = sum (map (strength vals) cycles)

pixel cycle val = if val >= cycle - 1 && val <= cycle + 1 then '#' else ' '
renderScreen vals = [pixel (cycle `mod` 40) val | (cycle, val) <- zip [0..] vals]
printLine screen line = print $ take 40 (drop (40 * line) screen)
printScreen screen = mapM_ (printLine screen) [0..5]

main = do
    commands <- parse <$> readFile "data/day10.txt"
    let vals = execute [1, 1] commands
    let part1 = sumStrength [20,60..220] vals
    print part1
    let screen = renderScreen vals
    printScreen screen