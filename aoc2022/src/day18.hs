import Data.List.Split (splitOn, chunksOf)
import Data.List (sortBy, nub)


type Cube = (Int, Int, Int)
type Border = (Cube, Cube)
type Bounds = ((Int, Int), (Int, Int), (Int, Int))


parse :: String -> [Cube]
parse input = [ (head (cube l), cube l !! 1, last (cube l)) | l <- lines input]
    where cube l = map read (splitOn "," l)


-- Part 1
searchBorders [] = []
searchBorders ((x, y, z):cs) = searchBorders cs ++ [((x, y, z), c) | c <- cs, adjacent c]
    where adjacent (x2, y2, z2) = (abs (x2 - x) + abs (y2 - y) + abs (z2 - z)) == 1 

countExposed cubes borders = (6 * length cubes) - (2 * length borders)


-- Part 2
lavaBounds :: [Cube] -> [Int] -> [Int] -> [Int] -> Bounds
lavaBounds [] xs ys zs = ((minimum xs, maximum xs), (minimum ys, maximum ys), (minimum zs, maximum zs))
lavaBounds ((x, y, z):cs) xs ys zs = lavaBounds cs (x:xs) (y:ys) (z:zs)

sortByZyx (x, y, z) (x2, y2, z2) = if z < z2 || (z == z2 && y < y2) || (z == z2 && y == y2 && x < x2) then LT else GT

getAir _ [] = []
getAir bounds ((x, y, z):sortedCubes)
    | null sortedCubes && y == by2 && z == bz2 = xToEnd
    | null sortedCubes && y == by2 = xToEnd ++ getAir bounds [(bx1 - 1, 0, z + 1)]
    | null sortedCubes = xToEnd ++ getAir bounds [(bx1 - 1, y + 1, z)]
    | y == y2 && z == z2 = [(ax, y, z) | ax <- [x+1..x2-1]] ++ getAir bounds sortedCubes
    | z == z2 = xToEnd ++ getAir bounds ((bx1 - 1, y+1, z) : sortedCubes)
    | y < by2 = xToEnd ++ getAir bounds ((bx1 - 1, y+1, z) : sortedCubes)
    | otherwise = xToEnd ++ getAir bounds ((bx1 - 1, by1, z+1) : sortedCubes)
    where 
        ((bx1, bx2), (by1, by2), (bz1, bz2)) = bounds
        (x2, y2, z2) = head sortedCubes
        xToEnd = [(ax, y, z) | ax <- [x+1..bx2]]

addBordersToGroup :: ([Border], [Border]) -> [Border] -> ([Border], [Border])
addBordersToGroup (group, []) remaining = (group, remaining)
addBordersToGroup (group, (b1, b2):bs) remaining
    | inGroup = addBordersToGroup ((b1, b2):group, bs) remaining
    | otherwise = addBordersToGroup (group, bs) ((b1, b2):remaining)
    where 
        isConnected (g1, g2) = g1 == b1 || g1 == b2 || g2 == b1 || g2 == b2
        inGroup = or [isConnected g | g <- group]

connectBorders :: [[Border]] -> [Border] -> [[Border]]
connectBorders groups [] = groups
connectBorders groups remaining
    | null (last groups) = connectBorders (init groups ++ [firstGroup]) firstRemaining
    | length nextRemaining == length remaining = connectBorders (groups ++ [firstGroup]) firstRemaining
    | otherwise = connectBorders (init groups ++ [nextGroup]) nextRemaining
    where 
        (newGroup:newBorders) = remaining
        (firstGroup, firstRemaining) = addBordersToGroup ([newGroup], newBorders) []
        (nextGroup, nextRemaining) = addBordersToGroup (last groups, remaining) []

uniqueCubes :: [[Border]] -> [[Cube]]
uniqueCubes borderGroups = [uniqueInGroup g | g <- borderGroups]
    where 
        uniqueInGroup :: [Border] -> [Cube]
        uniqueInGroup group = nub (concat [x:[y] | (x,y) <- group])

searchAlone [] _ = []
searchAlone ((x, y, z):cs) compare
    | isAlone = (x, y, z) : searchAlone cs compare
    | otherwise = searchAlone cs compare
    where 
        adjacent (x2, y2, z2) = (abs (x2 - x) + abs (y2 - y) + abs (z2 - z)) == 1
        isAlone = null ([c | c <- compare, adjacent c])

touchingBounds :: Bounds -> [Cube] -> Bool
touchingBounds bounds cubes = or touchingCubes
    where
        ((bx1, bx2), (by1, by2), (bz1, bz2)) = bounds
        touching (x, y, z) = x == bx1 || x == bx2 || y == by1 || y == by2 || z == bz1 || z == bz2
        touchingCubes = map touching cubes

getInterior :: Bounds -> [[Cube]] -> [[Cube]]
getInterior bounds groups = [g | g <- groups, not (touchingBounds bounds g)]


-- Charting
chart :: Bounds -> [Cube] -> [Cube] -> Int -> Int -> Int -> String
chart bounds cubes air x y z
    | z > bz2 = ""
    | y > by2 = ' ' : chart bounds cubes air nextX nextY nextZ
    | matchedC = '#' : chart bounds (tail cubes) air nextX nextY nextZ
    | matchedA = '+' : chart bounds cubes (tail air) nextX nextY nextZ
    | otherwise = '.' : chart bounds cubes air nextX nextY nextZ
    where
        ((bx1, bx2), (by1, by2), (bz1, bz2)) = bounds
        nextX = if x == bx2 then 0 else x + 1
        nextY = if x == bx2 then (if y > by2 then 0 else y + 1) else y
        nextZ = if x == bx2 && y == by2 then z + 1 else z
        matchedC = not (null cubes) && head cubes == (x, y, z)
        matchedA = not (null air) && head air == (x, y, z)
        
makeChart :: Bounds -> [Cube] -> [Cube] -> [[String]]
makeChart bounds cubes air = chunksOf (bz2 + 1) (chunksOf (by2 + 2) (chart bounds cubes air 0 0 0))
    where ((bx1, bx2), (by1, by2), (bz1, bz2)) = bounds


main = do
    cubes <- parse <$> readFile "data/day18.txt"
    let borders = searchBorders cubes

    -- Part 1
    let exposedSides = countExposed cubes borders
    print exposedSides

    -- Part 2
    let bounds = lavaBounds cubes [] [] []
    let sortedCubes = sortBy sortByZyx cubes
    let air = sortBy sortByZyx $ getAir bounds ((-1,0,0):sortedCubes)
    let airBorders = searchBorders air
    let aGroups = connectBorders [[]] airBorders
    let connectedAir = uniqueCubes aGroups
    let loneAir = searchAlone air air
    let allAir = connectedAir ++ [[a] | a <- loneAir]
    let bubbles = sortBy sortByZyx $ concat $ getInterior bounds allAir
    let cubesBubbles = sortBy sortByZyx $ cubes ++ bubbles
    let borders2 = searchBorders cubesBubbles
    let exposedSides2 = countExposed cubesBubbles borders2
    print exposedSides2
    
    -- Print ascii art
    let charted = makeChart bounds sortedCubes bubbles
    mapM_ (mapM_ print) charted
