import Data.List.Split (splitOn)
import Data.List (sortBy)
import Control.Parallel

type Point = (Int, Int)

distance :: Point -> Point -> Int
distance (x1, y1) (x2, y2) = abs (x1 - x2) + abs (y1 - y2)

data Sensor = Sensor { position :: Point, beacon :: Point, dist :: Int} deriving (Show)

makeSensor :: Point -> Point -> Sensor
makeSensor sPos bPos = Sensor { position = sPos, beacon = bPos, dist = distance sPos bPos }

parseLine :: String -> Sensor
parseLine x = makeSensor sPos bPos
    where coords = map (map (read . last . splitOn "=") . splitOn ",") (splitOn ":" x)
          sPos = (head (head coords), last (head coords))
          bPos = (head (last coords), last (last coords))

parse :: String -> [Sensor]
parse input = [parseLine x | x <- lines input]


-- Part 1
pointsOnRow :: Int -> Sensor -> (Int, Int)
pointsOnRow row sensor = if pointDist <= dist sensor then (x - radius, x + radius) else (0, 0)
    where (x, y) = position sensor
          point = (x, row)
          pointDist = abs (y - row)
          radius = dist sensor - pointDist

removeItem _ [] = []
removeItem x (y:ys) 
    | x == y    = removeItem x ys
    | otherwise = y : removeItem x ys

sortByLeft x1 x2 = if fst x1 < fst x2 then LT else GT

combineOverlaps [] = []
combineOverlaps [x] = [x]
combineOverlaps ((xl, xr):ys)
    | yl <= xr && xr > yr = combineOverlaps ((xl, xr) : tail ys)
    | yl <= xr = combineOverlaps ((xl, yr) : tail ys)
    | otherwise = (xl, xr) : combineOverlaps ys
    where (yl, yr) = head ys

sumRowRange [(xl, xr)] = xr - xl
sumRowRange ((xl, xr): ys) = (xr - xl) + sumRowRange ys


-- Part 2
tuningFreq (row, points) = 4000000 * (snd (head points) + 1) + row

scan row stop sensors
    | row == stop = 0
    | length points > 1 = tuningFreq (row, points)
    | otherwise = 0 + scan (row - 1) stop sensors
    where points = combineOverlaps (sortBy sortByLeft (removeItem (0, 0) (map (pointsOnRow row) sensors)))

-- For multithreading
runScan sensors 0 stop = 0
runScan sensors start stop = par r (s + r)
    where s = scan start stop sensors
          r = runScan sensors (start - 200000) (stop - 200000)


main = do
    sensors <- parse <$> readFile "data/day15.txt"
    let points2mil = sortBy sortByLeft (removeItem (0, 0) (map (pointsOnRow 2000000) sensors))
    let combinedPoints = combineOverlaps points2mil
    let sum2mil = sumRowRange combinedPoints
    print sum2mil
    let freq = runScan sensors 4000000 3800000
    print freq