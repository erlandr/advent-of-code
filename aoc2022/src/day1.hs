import Data.List

strToInt :: String -> Int
strToInt s = if s == "" then 0 else read s

findZero :: Int -> Int -> Bool
findZero _ y = y /= 0

totals :: [[Int]] -> [Int]
totals x = reverse (sort (map sum x))

parse :: String -> [Int]
parse x = totals (groupBy findZero (map strToInt (lines x)))

top :: Int -> [Int] -> Int
top x l = sum (take x l)

main :: IO ()
main = do  
        contents <- readFile "data/day1.txt"
        let calories = parse contents
        print (top 1 calories)
        print (top 3 calories)