import Data.List.Split (split, oneOf)
import Data.Maybe (Maybe, isNothing, isJust, fromJust)
import Data.List (find)
import Codec.Picture

right = 0
down  = 1
left  = 2
up    = 3

void = ' '
flat = '.'
wall = '#'

findFirst :: Num a => [Char] -> a
findFirst (c:cs) = if c /= ' ' then 0 else 1 + findFirst cs

-- (x, y, facing)
type Position = (Int, Int, Int)

nextCoord board (x, y, facing)
    | facing == right = if x == maxX then (minX, y, facing) else (x + 1, y, facing)
    | facing == left  = if x == minX then (maxX, y, facing) else (x - 1, y, facing)
    | facing == down  = if y == maxY then (x, minY, facing) else (x, y + 1, facing)
    | facing == up    = if y == minY then (x, maxY, facing) else (x, y - 1, facing)
    where (minX, maxX, minY, maxY) = (0, length (head board) - 1, 0, length board - 1)

wrapBoard :: [String] -> Position -> Position
wrapBoard board pos
    | tile == void = wrapBoard board (nextX, nextY, facing)
    | otherwise = (nextX, nextY, facing)
    where 
        (nextX, nextY, facing) = nextCoord board pos
        tile = board !! nextY !! nextX

wrapCube :: [String] -> Position -> Position
wrapCube board (x, y, facing)
    | zone == (1, 0) && nextZone == (0, 0) = (0, 149 - y, right)    -- Top to Left
    | zone == (1, 0) && nextZone == (1, 3) = (0, x + 100, right)    -- Top to Back
    | zone == (2, 0) && nextZone == (0, 0) = (99, 149 - y, left)    -- Right to Bottom
    | zone == (2, 0) && nextZone == (2, 1) = (99, x - 50, left)     -- Right to Front
    | zone == (2, 0) && nextZone == (2, 3) = (x - 100, 199, up)     -- Right to Back
    | zone == (1, 1) && nextZone == (2, 1) = (y + 50, 49, up)       -- Front to Right
    | zone == (1, 1) && nextZone == (0, 1) = (y - 50, 100, down)    -- Front to Left
    | zone == (0, 2) && nextZone == (2, 2) = (50, 149 - y, right)   -- Left to Top
    | zone == (0, 2) && nextZone == (0, 1) = (50, x + 50, right)    -- Left to Front
    | zone == (1, 2) && nextZone == (2, 2) = (149, 149 - y, left)   -- Bot to Right
    | zone == (1, 2) && nextZone == (1, 3) = (49, x + 100, left)    -- Bot to Back
    | zone == (0, 3) && nextZone == (1, 3) = (y - 100, 149, up)     -- Back to Bot
    | zone == (0, 3) && nextZone == (0, 0) = (x + 100, 0, down)     -- Back to Right
    | zone == (0, 3) && nextZone == (2, 3) = (y - 100, 0, down)     -- Back to Top
    | otherwise = (nextX, nextY, facing)
    where 
        (nextX, nextY, _) = nextCoord board (x, y, facing)
        zone = (x `div` 50, y `div` 50)
        nextZone = (nextX `div` 50, nextY `div` 50)

follow :: ([String] -> Position -> Position) -> [String] -> Position -> [String] -> [Position]
follow fNextPos board pos [] = [pos]
follow fNextPos board pos (p:ps)
    | p == "L" = follow fNextPos board (x, y, turnLeft) ps
    | p == "R" = follow fNextPos board (x, y, turnRight) ps
    | read p > 0 = moveForward : follow fNextPos board moveForward (show (read p - 1):ps)
    | otherwise = follow fNextPos board pos ps
    where
        (x, y, facing) = pos
        turnLeft = if facing == 0 then 3 else facing - 1
        turnRight = if facing == 3 then 0 else facing + 1
        (nextX, nextY, nextFacing) = fNextPos board pos
        nextTile = board !! nextY !! nextX
        moveForward = if nextTile == wall then pos else (nextX, nextY, nextFacing)
  
password (x, y, facing) = 1000 * (y + 1) + 4 * (x + 1) + facing

makeChart :: [String] -> [Position] -> Int -> Int -> PixelRGB8
makeChart board path x y
    | isJust p && facing == right   = PixelRGB8 255 55 25
    | isJust p && facing == down    = PixelRGB8 195 105 55
    | isJust p && facing == left    = PixelRGB8 155 55 105
    | isJust p && facing == up      = PixelRGB8 195 25 55
    | c == flat                     = PixelRGB8 35 35 35
    | c == wall                     = PixelRGB8 15 240 15
    | otherwise                     = PixelRGB8 0 0 0
    where
        c = board !! y !! x
        p = find (\(px, py, _) -> x == px && y == py) path
        (_, _, facing) = fromJust p

main = do
    input <- lines <$> readFile "data/day22.txt"
    let board = init (init input)
    let longest = maximum [length row | row <- board]
    let fixedBoard = [take longest (row ++ repeat ' ') | row <- board]
    let path = split (oneOf "RL") (last input)
    let startPos = (findFirst (head fixedBoard), 0, right)

    -- Part 1
    let plot = follow wrapBoard fixedBoard startPos path
    let endPos = last plot
    print endPos
    print (password endPos)

    -- Part 2
    let plot2 = follow wrapCube fixedBoard startPos path
    let endPos2 = last plot2
    print endPos2
    print (password endPos2)

    -- Charting
    let chart = makeChart fixedBoard plot2
    savePngImage "data/day22_board.png" (ImageRGB8 (generateImage chart 150 200))