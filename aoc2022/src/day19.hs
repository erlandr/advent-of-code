import Data.List.Split (splitOn, chunksOf)
import Debug.Trace
import Control.Monad


data Material = Material { ore :: Int, clay :: Int, obs :: Int, geodes :: Int } deriving (Show)
add :: Material -> Material -> Material
add a b = Material { ore = ore a + ore b, clay = clay a + clay b, obs = obs a + obs b, geodes = geodes a + geodes b }
sub :: Material -> Material -> Material
sub a b = Material { ore = ore a - ore b, clay = clay a - clay b, obs = obs a - obs b, geodes = geodes a - geodes b }

data Blueprint = Blueprint { oreRobo :: Material, clayRobo :: Material, obsRobo :: Material, geoRobo :: Material } deriving (Show)

parse input = map parseLine (lines input)
    where 
        parseLine l = makeBp $ map (makeCost . last . splitOn " robot costs ") (splitOn ". " (last (splitOn ": " l)))
        makeBp (ore:clay:obs:geo:xs) = Blueprint { oreRobo = ore, clayRobo = clay, obsRobo = obs, geoRobo = geo }
        makeCost costStr = combine costChunks            
            where 
                combine [m] = m
                combine (m:ms) = combine (m `add` head ms : tail ms)
                costChunks = map (convert . splitOn " ") (splitOn " and " costStr)
                convert chunk
                    | last chunk == "ore" = Material { ore = read (head chunk), clay = 0, obs = 0, geodes = 0 }
                    | last chunk == "clay" = Material { ore = 0, clay = read (head chunk), obs = 0, geodes = 0 }
                    | otherwise = Material { ore = 0, clay = 0, obs =  read (head chunk), geodes = 0 }
                

type Strategy = [Int]

data Factory = Factory { bp :: Blueprint, strategy :: Strategy, material :: Material, oreRobos :: Int, clayRobos :: Int, obsRobos :: Int, geoRobos :: Int } deriving (Show)
makeFactory bp strategy = Factory { bp = bp, strategy = strategy, material = Material { ore = 0, clay = 0, obs = 0, geodes = 0 }, oreRobos = 1, clayRobos = 0, obsRobos = 0, geoRobos = 0 }

build factory
    | strat == 3 && canAfford costGeoRobo = buildGeoRobo
    | strat == 2 && canAfford costObsRobo = buildObsRobo
    | strat == 1 && canAfford costClayRobo = buildClayRobo
    | strat == 0 && canAfford costOreRobo = buildOreRobo
    | otherwise = factory
    where
        strat = if not (null (strategy factory)) then head (strategy factory) else 3
        canAfford cost = (ore (material factory) - ore cost >= 0) && (clay (material factory) - clay cost >= 0) && (obs (material factory) - obs cost >= 0)
        numOreRobos = oreRobos factory
        costOreRobo = oreRobo (bp factory)
        numClayRobos = clayRobos factory
        costClayRobo = clayRobo (bp factory)
        numObsRobos = obsRobos factory
        costObsRobo = obsRobo (bp factory)
        numGeoRobos = geoRobos factory
        costGeoRobo = geoRobo (bp factory)
        nextStrategy = if not (null (strategy factory)) then tail (strategy factory) else [3]
        buildGeoRobo = factory { material = material factory `sub` costGeoRobo, geoRobos = numGeoRobos + 1, strategy = nextStrategy }
        buildObsRobo = factory { material = material factory `sub` costObsRobo, obsRobos = numObsRobos + 1, strategy = nextStrategy }
        buildClayRobo = factory { material = material factory `sub` costClayRobo, clayRobos = numClayRobos + 1, strategy = nextStrategy }
        buildOreRobo = factory { material = material factory `sub` costOreRobo, oreRobos = numOreRobos + 1, strategy = nextStrategy }

dig factory = newMaterial
    where newMaterial = Material { ore = oreRobos factory, clay = clayRobos factory, obs = obsRobos factory, geodes = geoRobos factory }

simulate factory 0 = factory
simulate factory t = simulate nextFactory (t - 1)
    where
        buildResults = build factory
        digResults = dig factory
        nextFactory = buildResults { material = material buildResults `add` digResults }

searchBos bp [] best minutes = trace (show best) best
searchBos bp bos best minutes = searchBos bp (tail bos) nextBest minutes
    where
        factory = makeFactory bp (head bos)
        simulated = simulate factory minutes
        -- nextBest = max (geodes (material simulated)) best
        nextBest = if geodes (material simulated) > best then trace (show best ++ show (head bos)) geodes (material simulated) else best


main = do
    blueprints <- parse <$> readFile "data/day19.txt"
    -- let factory = makeFactory (head blueprints) [1,1,1,2,1,2,3,3]
    -- print factory
    -- let sf = simulate factory 24
    -- let msf = material sf
    -- let robos = (oreRobos sf, clayRobos sf, obsRobos sf, geoRobos sf)
    -- print ""
    -- print ("Final material: " ++ show msf)
    -- print robos
    -- let f2 = makeFactory (last blueprints) [1,0,1,2,2,2,3,3]
    -- let sf2 = simulate f2 24
    -- print (material sf2)

    -- let f1 = makeFactory (head blueprints) [1, 1,1,2, 1,2,3,3]
    -- let sim1 = simulate f1 24
    -- print sim1

    -- let f = makeFactory (last blueprints) [0, 0,1,1, 1,1,1,2,2,2, 2,2,3,2,3,3,3,3]
    -- let sim = simulate f 24
    -- print sim

    -- Current best: [5,0,2,10,3,6,0,11,3,8,1,0,1,0,15,0,0,12,0,1,1,0,3,13,1,1,2,0,9,1]

    -- 1: [0, 0,0,1, 1,1,1,1,1,1, 2,2,2,3,2,3] 
    -- 2: always 0
    -- 3: [0, 1,1,1, 1,2,2,1,3,1, 2,2,2,2,2,3]
    -- 4: [0, 0,1,1, 1,1,1,1,1,2, 2,2,2,3,2,3]
    -- 5: [0, 0,0,1, 1,1,1,1,1,2, 2,2,3,2,2,3]
    -- 6: [0, 0,1,0, 1,1,1,1,1,2, 2,2,2,2,3,3]
    -- 7: always 0?

    {- 
     1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9
     1   2       3
             0             1         2
    [0,0,0,1,1,1,1,1,1,1,2,2,2,3,2,3,3]
    [0,1,1,1,1,2,2,1,3,1,1,2,2,2,2,2,3]
    [0,0,1,1,1,1,1,1,1,2,2,2,2,3,2,3,3]
    [0,0,0,1,1,1,1,1,1,2,1,2,2,3,2,2,3]
    [0,0,1,0,1,1,1,1,1,2,2,2,2,2,3,2,3]
    [1,1,2,2,0,2,3,3,1,3,1,2,2,2,2,2,3]    
    [0,0,0,1,1,1,1,1,1,1,1,2,2,2,2,3,3]
    [0,0,1,0,1,1,1,2,1,2,2,2,2,3,2,2,3]
    [0,0,1,1,1,1,1,2,2,1,2,2,2,3,2,2,3]
    [0,0,0,1,1,1,1,1,1,2,2,2,2,2,3,2,3]
    [0,0,1,1,1,1,1,1,2,2,2,2,2,3,3,2,3]
    [0,1,0,1,1,1,2,1,2,2,1,2,3,2,3,2,3]
    [0,0,0,1,0,1,1,1,1,1,1,2,2,2,3,2,3]
    [0,0,1,1,1,1,1,1,1,1,2,2,2,2,3,2,3]
    [0,0,1,1,1,1,1,1,2,1,2,2,3,2,2,2,3]
    [0,1,1,1,1,1,2,1,2,2,2,3,2,2,3,2,3]
    [0,0,1,1,1,1,1,1,1,1,2,2,2,2,3,2,3]
    [0,0,1,1,1,1,1,1,1,1,1,2,2,2,2,2,3]
    [0,0,0,1,1,1,1,1,1,1,2,2,3,2,2,2,3]
    [0,1,1,1,0,2,2,1,2,3,2,3,3,2,2,2,3]
    [0,0,1,1,1,1,1,1,1,1,2,2,2,3,2,2,3]
    [0,1,1,1,0,2,2,1,2,3,2,1,3,3,2,2,2,3]
    [0,1,1,1,1,2,2,1,2,3,2,2,3,3,2,2,2,3]

    [0,0,0,1,1,1,1,1,1,1,2,1,2,2,2,3,2,3,3]
    [0,0,1,0,1,1,1,1,1,2,1,2,1,2,2,2,3,2,3]
    [0,0,1,1,1,1,1,1,2,2,2,2,3,2,3,2,3,3]
    -}
    
    -- let bos1 = replicateM 2 [0, 1]
    -- let bos2 = replicateM 3 [0, 1, 2]
    -- let bos3 = replicateM 2 [1, 2]
    -- let bos4 = replicateM 5 [1, 2, 3]
    -- let bos5 = replicateM 5 [2, 3]
    -- let bosAll = concat $ concat $ concat $ concat [[[[[b1 ++ b2 ++ b3 ++ b4 ++ b5 ++ [3] | b5 <- bos5] | b4 <- bos4] | b3 <- bos3] | b2 <- bos2] | b1 <- bos1]
    -- let bos = bosAll
    -- print (length bosAll)

    let bos1 = replicateM 2 [0, 1]
    let bos2 = replicateM 3 [0, 1, 2]
    let bos3 = replicateM 6 [1, 2]
    let bos4 = replicateM 3 [1, 2, 3]
    let bos5 = replicateM 5 [2, 3]
    let bosAll = concat $ concat $ concat $ concat [[[[[b1 ++ b2 ++ b3 ++ b4 ++ b5 ++ [3] | b5 <- bos5] | b4 <- bos4] | b3 <- bos3] | b2 <- bos2] | b1 <- bos1]
    let bos = bosAll
    print (length bosAll)

    let results = searchBos (blueprints !! 0) bos 0 32
    print results

    let results2 = searchBos (blueprints !! 1) bos 0 32
    print results2

    let results3 = searchBos (blueprints !! 2) bos 0 32
    print results3

    -- let optimals = [searchBos bp bos 0 | bp <- blueprints]
    -- print optimals
    -- let results = sum [x * i | (x, i) <- zip optimals [1..]]
    -- print results