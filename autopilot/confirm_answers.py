import os
import io
import time
import csv
from openai import OpenAI
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
from dataclasses import dataclass


@dataclass
class Aoc:
    part1: str
    part2: str
    data: str
    answer1: str
    answer2: str


def get_aoc(year, day):
    """ Scrape the Advent of Code website for the data and answers """
    options = Options()
    options.add_argument("--headless=new")
    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=options)

    url = f"https://adventofcode.com/{year}/day/{day}"
    url_input = f"{url}/input"

    session_key = os.environ.get("AOC_SESSION_KEY")
    session_cookie = {"name": "session", "value": session_key}

    # Setup cookies to login
    driver.get(url)
    driver.add_cookie(session_cookie)

    # Get part 1 and 2 (if it exists)
    driver.get(url)
    content = WebDriverWait(driver, 10).until(EC.visibility_of_any_elements_located((By.TAG_NAME, "article")))
    part1 = content[0].text
    part2 = content[1].text if len(content) > 1 else None

    # Get answers
    content = driver.find_elements(By.TAG_NAME, "p")
    answers = []
    for c in content:
        if "Your puzzle answer was" in c.text:
            code_ele = c.find_element(By.TAG_NAME, "code")
            answers.append(code_ele.text)

    # Get input data
    driver.get(url_input)
    content = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.TAG_NAME, "pre")))
    data = content.text

    driver.quit()

    return Aoc(part1, part2, data, answers[0], answers[1])


def create_file(client, data):
    """ Upload file to OpenAI, return file ID """
    stream = io.BytesIO(data.encode("utf-8"))
    file = client.files.create(
        file=stream,
        purpose="assistants"
    )
    return file.id


def create_thread(client):
    """ Create thread, return thread ID """
    thread = client.beta.threads.create()
    return thread.id


def add_message(client, thread_id, file_id, text):
    """ Add message to thread, return message ID """
    message = client.beta.threads.messages.create(
        thread_id,
        role="user",
        content=text,
        file_ids=[file_id]
    )
    return message.id


def create_run(client, assistant_id, thread_id):
    """ Create a run and return it """
    run = client.beta.threads.runs.create(
        thread_id=thread_id,
        assistant_id=assistant_id
    )
    return run


def wait_for_run_complete(client, thread_id, run):
    """ Check run status every second until no longer in_progress """
    while run.status in ["queued", "in_progress"]:
        run = client.beta.threads.runs.retrieve(
            thread_id=thread_id,
            run_id=run.id
        )
        if run.status == "in_progress":
            print(".", end="")
        else:
            print(run.status)
        time.sleep(1)
    return run.status


def get_assistant_answer(client, thread_id, run_id):
    """ Find answer in run steps and return it as a string """
    answer = ""
    run_steps = client.beta.threads.runs.steps.list(
        thread_id=thread_id,
        run_id=run_id
    )
    for r in run_steps.data:
        if r.type == "tool_calls":
            outputs = r.step_details.tool_calls[0].code_interpreter.outputs
            if len(outputs) > 0:
                answer = outputs[0].logs
                break
    return str(answer)


def cleanup(client, file_id, thread_id):
    """ Delete file, thread and all messages and runs """
    client.beta.threads.delete(thread_id)
    client.files.delete(file_id)


def compute_day(assistant_id, year, day):
    """ Use the assistant to complete parts 1 and 2 of the Advent of Code puzzle """
    aoc = get_aoc(year, day)

    results = {
        "year": year,
        "day": day,
        "answer1": "",
        "expected1": aoc.answer1,
        "correct1": False,
        "answer2": "",
        "expected2": aoc.answer2,
        "correct2": False
    }

    client = OpenAI()

    file_id = create_file(client, aoc.data)
    thread_id = create_thread(client)

    # Part 1
    add_message(client, thread_id, file_id, aoc.part1)
    run = create_run(client, assistant_id, thread_id)
    final_status = wait_for_run_complete(client, thread_id, run)
    if final_status == "completed":
        results["answer1"] = get_assistant_answer(client, thread_id, run.id)
        results["correct1"] = results["answer1"] == results["expected1"]
        if results["correct1"]:
            print(f"Part 1 CORRECT assistant: {results['answer1']}, correct: {aoc.answer1}")
        else:
            print(f"Part 1 WRONG   assistant: {results['answer1']}, correct: {aoc.answer1}")

    if results["correct1"] and aoc.part2 and aoc.answer2:
        # Part 2
        add_message(client, thread_id, file_id, aoc.part2)
        run = create_run(client, assistant_id, thread_id)
        final_status = wait_for_run_complete(client, thread_id, run)
        if final_status == "completed":
            results["answer2"] = get_assistant_answer(client, thread_id, run.id)
            results["correct2"] = results["answer2"] == results["expected2"]
            if results["correct2"]:
                print(f"Part 2 CORRECT assistant: {results['answer2']}, correct: {aoc.answer2}")
            else:
                print(f"Part 2 WRONG   assistant: {results['answer2']}, correct: {aoc.answer2}")
    
    cleanup(client, file_id, thread_id)

    return results

def create_new_results_file(year):
    """ Create a new results file and return its file name """
    filename = f"aoc_results_{year}.csv"
    file_path = os.path.join("autopilot", filename)
    # Check if file exists
    if os.path.exists(file_path):
        return file_path
    # Create file
    with open(file_path, "w") as f:
        writer = csv.DictWriter(f, fieldnames=["year", "day", "answer1", "expected1", "correct1", "answer2", "expected2", "correct2"], lineterminator="\n")
        writer.writeheader()
    return file_path


def save_results(results, filename):
    """ Append results to a csv file """
    with open(filename, "a") as f:
        writer = csv.DictWriter(f, fieldnames=results.keys(), lineterminator="\n")
        writer.writerow(results)


def main(assistant_id, year, start_day):
    """ Run the assistant for all 25 days of Advent of Code """
    results_file = create_new_results_file(year)
    for day in range(start_day, 26):
        day_results = compute_day(assistant_id, year, day)
        save_results(day_results, results_file)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-y", "--year", help="The year to complete", required=True)
    parser.add_argument("-d", "--day", help="The day to start on", required=False, default=1)
    args = parser.parse_args()

    assistant_id = os.environ.get("ASSISTANT_ID")
    main(assistant_id, int(args.year), int(args.day))
