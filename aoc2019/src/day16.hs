import Data.Char(digitToInt)
import Data.String.Utils(strip)

makePattern :: Int -> [Int]
makePattern n = drop 1 (cycle (concatMap (<$ [1..n]) [0, 1, 0, -1]))

calcDigit :: [Int] -> Int -> Int
calcDigit signal n = abs (sum (zipWith (*) signal (makePattern n))) `mod` 10

calcFft :: [Int] -> Int -> Int
calcFft signal 0 = read $ concatMap show (take 8 signal)
calcFft signal times = calcFft (map (calcDigit signal) [1..(length signal)]) (times - 1)

parse :: String -> [Int]
parse x = map digitToInt (strip x)

main = do  
        transmission <- readFile "data/day16.txt"
        print (calcFft (parse transmission) 100)