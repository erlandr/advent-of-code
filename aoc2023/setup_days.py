import os
import shutil


def setup_day(day: int):
    day_path = f"day{day}"
    if not os.path.exists(day_path):
        os.mkdir(day_path)
    
    example_path = os.path.join(day_path, "example1.txt")
    # check if file exists
    if not os.path.exists(example_path):
        with open(example_path, "w") as f:
            f.write("")

    template_path = "template.ipynb"
    day_notebook_path = os.path.join(day_path, f"day{day}.ipynb")
    if not os.path.exists(day_notebook_path):
        shutil.copy(template_path, day_notebook_path)


def setup_all_days():
    for i in range(1, 26):
        setup_day(i)


if __name__ == "__main__":
    setup_all_days()
