use std::fs;
use std::time::Instant;
use std::collections::HashMap;
use lazy_static::lazy_static;
use itertools::Itertools;

lazy_static! {
    static ref KEYPAD: HashMap<char, (usize, usize)> = {
        let mut m = HashMap::new();
        m.insert('7', (0, 0));
        m.insert('8', (1, 0));
        m.insert('9', (2, 0));
        m.insert('4', (0, 1));
        m.insert('5', (1, 1));
        m.insert('6', (2, 1));
        m.insert('1', (0, 2));
        m.insert('2', (1, 2));
        m.insert('3', (2, 2));
        m.insert('0', (1, 3));
        m.insert('A', (2, 3));
        m
    };
    
    static ref DIR_KEYS: HashMap<char, (usize, usize)> = {
        let mut m = HashMap::new();
        m.insert('^', (1, 0));
        m.insert('A', (2, 0));
        m.insert('<', (0, 1));
        m.insert('v', (1, 1));
        m.insert('>', (2, 1));
        m
    };
}

fn main() {
    let start = Instant::now();

    println!("Advent of Code 2024 - Day 21");
    println!();
    println!("Example Part 1: {}", solve("data/example.txt", 2));
    println!();
    println!("Part 1: {}", solve("data/input.txt", 2));
    println!("Part 2: {}", solve("data/input.txt", 25));
    println!();

    let duration = start.elapsed();
    println!("Time elapsed: {:?}", duration);
}

fn parse(path: &str) -> Vec<Vec<char>> {
    let content = fs::read_to_string(path).unwrap();
    content.lines().map(|l| l.chars().collect()).collect()
}

fn solve(path: &str, iterations: usize) -> usize {
    let codes = parse(path);
    let mut sum = 0;
    let mut memo = HashMap::new();
    for code in &codes {
        let instructions = enter_code(code);
        let mut low_total = usize::MAX;
        for instr in instructions {
            let i = enter_dirs(&instr, iterations, &mut memo);
            let total: usize = i.into_iter().map(|(_, n)| n).sum();
            if total < low_total {
                low_total = total;
            }
        }
        sum += get_num(code) * low_total;
        println!("{} * {}", low_total, get_num(code));
    }
    sum
}

fn get_num(code: &Vec<char>) -> usize {
    let mut num = 0;
    for c in code {
        if c == &'A' {
            continue;
        }
        num = num * 10 + c.to_digit(10).unwrap() as usize;
    }
    num
}

type MoveFn = fn(&mut usize, &mut usize, &mut isize, &mut isize, &mut Vec<char>, (usize, usize));

fn move_left(x: &mut usize, y: &mut usize, dx: &mut isize, _dy: &mut isize, instructions: &mut Vec<char>, avoid: (usize, usize)) {
    if *dx < 0 && ((*x as isize + *dx) as usize, *y) != avoid {
        for _ in 0..dx.abs() {
            *x -= 1;
            instructions.push('<');
        }
        *dx = 0;
    }
}

fn move_right(x: &mut usize, _y: &mut usize, dx: &mut isize, _dy: &mut isize, instructions: &mut Vec<char>, _avoid: (usize, usize)) {
    if *dx > 0 {
        for _ in 0..dx.abs() {
            *x += 1;
            instructions.push('>');
        }
        *dx = 0;
    }
}

fn move_down(x: &mut usize, y: &mut usize, _dx: &mut isize, dy: &mut isize, instructions: &mut Vec<char>, avoid: (usize, usize)) {
    if *dy > 0 && (*x, *y + (*dy as usize)) != avoid {
        for _ in 0..dy.abs() {
            *y += 1;
            instructions.push('v');
        }
        *dy = 0;
    }
}

fn move_up(x: &mut usize, y: &mut usize, _dx: &mut isize, dy: &mut isize, instructions: &mut Vec<char>, avoid: (usize, usize)) {
    if *dy < 0 && (*x, (*y as isize + *dy) as usize) != avoid {
        for _ in 0..dy.abs() {
            *y -= 1;
            instructions.push('^');
        }
        *dy = 0;
    }
}

fn enter_code(code: &Vec<char>) -> Vec<Vec<char>> {
    let mut all_instructions: Vec<Vec<char>> = Vec::new();
    let movements = [
        move_left as fn(&mut usize, &mut usize, &mut isize, &mut isize, &mut Vec<char>, (usize, usize)),
        move_down,
        move_right,
        move_up,
    ];

    for perm in movements.iter().permutations(4) {
        let mut instructions: Vec<char> = Vec::new();
        let (mut x, mut y) = KEYPAD.get(&'A').unwrap();
        for c in code {
            let (tx, ty) = KEYPAD.get(c).unwrap();
            let mut dx = *tx as isize - x as isize;
            let mut dy = *ty as isize - y as isize;
            while dx != 0 || dy != 0 {
                for &move_fn in &perm {
                    move_fn(&mut x, &mut y, &mut dx, &mut dy, &mut instructions, (0, 3));
                }
            }
            instructions.push('A');
        }
        all_instructions.push(instructions);
    }

    all_instructions
}

fn generate_instructions(prev_char: char, target_char: char, move_fns: Vec<MoveFn>) -> Vec<char> {
    let (mut x, mut y) = DIR_KEYS.get(&prev_char).unwrap();
    let (tx, ty) = DIR_KEYS.get(&target_char).unwrap();
    let mut dir_instructions: Vec<char> = Vec::new();
    let mut dx = *tx as isize - x as isize;
    let mut dy = *ty as isize - y as isize;
    while dx != 0 || dy != 0 {
        for &move_fn in &move_fns {
            move_fn(&mut x, &mut y, &mut dx, &mut dy, &mut dir_instructions, (0, 0));
        }
    }
    dir_instructions.push('A');
    dir_instructions
}

fn enter_dirs(dirs: &Vec<char>, iterations: usize, memo: &mut HashMap<(char, char, usize), usize>) -> Vec<(char, usize)> {
    let mut instructions: Vec<(char, usize)> = Vec::new();
    let mut prev_char = 'A';
    for d in dirs {
        if let Some(total) = memo.get(&(prev_char, *d, iterations)) {
            instructions.push((*d, *total));
            prev_char = *d;
            continue;
        }
        
        let dir_instructions = generate_instructions(prev_char, *d, vec![move_left, move_right, move_down, move_up]);
        let dir_instructions2 = generate_instructions(prev_char, *d, vec![move_down, move_up, move_left, move_right]);
    
        if iterations > 1 {
            let recur1 = enter_dirs(&dir_instructions, iterations - 1, memo);
            let total1: usize = recur1.into_iter().map(|(_, n)| n).sum();
            let recur2 = enter_dirs(&dir_instructions2, iterations - 1, memo);
            let total2 = recur2.into_iter().map(|(_, n)| n).sum();
            let total = total1.min(total2);
            instructions.push((*d, total));
            memo.insert((prev_char.clone(), *d, iterations), total);
        } else {
            instructions.push((*d, dir_instructions.len()));
        }
        prev_char = *d;
    }
    instructions
}
