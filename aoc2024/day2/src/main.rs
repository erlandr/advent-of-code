use std::fs;
use std::time::Instant;

fn main() {
    let start = Instant::now();

    println!("Advent of Code 2024 - Day 2");
    println!();
    println!("Example Part 1: {}", part1("data/example.txt"));
    println!("Example Part 2: {}", part2("data/example.txt"));
    println!();
    println!("Part 1: {}", part1("data/input.txt"));
    println!("Part 2: {}", part2("data/input.txt"));
    println!();

    let duration = start.elapsed();
    println!("Time elapsed: {:?}", duration);
}

fn parse(data_path: &str) -> Vec<Vec<i32>> {
    let data: String = fs::read_to_string(data_path).unwrap();
    let mut reports: Vec<Vec<i32>> = Vec::new();
    for line in data.lines() {
        let levels: Vec<&str> = line.split_whitespace().collect();
        let mut report: Vec<i32> = Vec::new();
        for level in levels {
            report.push(level.parse().unwrap());
        }
        reports.push(report);
    }
    reports
}

fn part1(data_path: &str) -> i32 {
    let reports = parse(data_path);
    let mut safe_reports = 0;
    for report in &reports {
        safe_reports += test_report(report);
    }
    safe_reports
}

fn test_report(report: &Vec<i32>) -> i32 {
    // Test if report is safe, return 1 if safe, 0 if not
    let mut prev_level = report[0];
    let direction = get_direction(report);
    for level in &report[1..] {
        if (direction == 1 && *level > prev_level) || (direction == -1 && *level < prev_level) {
            let diff = (level - prev_level).abs();
            if diff > 3 || diff == 0 {
                return 0;
            }
        } else {
            return 0;
        }
        prev_level = *level;
    }
    1
}

fn get_direction(report: &Vec<i32>) -> i32 {
    if report.first() < report.last() {
        1
    } else {
        -1
    }
} 

fn part2(data_path: &str) -> i32 {
    let reports = parse(data_path);
    let mut safe_reports = 0;
    for report in &reports {
        for damp_rep in report_dampener(report) {
            if test_report(&damp_rep) == 1 {
                safe_reports += 1;
                break;
            }
        }
    }
    safe_reports
}



fn report_dampener(report: &Vec<i32>) -> Vec<Vec<i32>> {
    // return all possible reports with one level removed
    let mut reports: Vec<Vec<i32>> = Vec::new();
    reports.push(report.clone());
    for i in 0..report.len() {
        let mut new_report = report.clone();
        new_report.remove(i);
        reports.push(new_report);
    }
    reports
}