use std::fs;
use std::time::Instant;
use std::collections::{HashMap, HashSet};

fn main() {
    let start = Instant::now();

    println!("Advent of Code 2024 - Day 22");
    println!();
    println!("Example Part 1: {}", part1("data/example.txt"));
    println!("Example Part 2: {}", part2("data/example2.txt"));
    println!();
    println!("Part 1: {}", part1("data/input.txt"));
    println!("Part 2: {}", part2("data/input.txt"));
    println!();

    let duration = start.elapsed();
    println!("Time elapsed: {:?}", duration);
}

fn parse(path: &str) -> Vec<isize> {
    fs::read_to_string(path)
        .unwrap()
        .lines()
        .map(|line: &str| {
            line.parse().unwrap()
        })
        .collect()
}

fn process(secret: isize) -> isize {
    let mut secret = secret;
    let mut val = secret * 64;
    val = val ^ secret;
    secret = val % 16777216;
    val = secret / 32;
    val = val ^ secret;
    secret = val % 16777216;
    val = secret * 2048;
    val = val ^ secret;
    val % 16777216
}

fn part1(path: &str) -> isize {
    let mut sum = 0;
    let secrets = parse(path);
    for i in 0..secrets.len() {
        let mut secret = secrets[i];
        for _ in 0..2000 {
            secret = process(secret);
        }
        sum += secret;
    }
    sum
}

fn part2(path: &str) -> isize {
    let secrets = parse(path);
    let mut sell_triggers: HashMap<(isize, isize, isize, isize), isize> = HashMap::new();
    for i in 0..secrets.len() {
        let mut rolling_changes = [0; 4];
        let mut visited_triggers: HashSet<(isize, isize, isize, isize)> = HashSet::new();
        let mut secret = secrets[i];
        let mut prev_price = secret % 10;
        for j in 0..2000 {
            secret = process(secret);
            let price = secret % 10;
            rolling_changes.rotate_left(1);
            rolling_changes[3] = price - prev_price;
            if j > 2 {
                let trigger = (rolling_changes[0], rolling_changes[1], rolling_changes[2], rolling_changes[3]);
                if visited_triggers.insert(trigger) {
                    let entry = sell_triggers.entry(trigger).or_insert(0);
                    *entry += price;
                }
            }
            prev_price = price;
        }
    }
    *sell_triggers.values().max().unwrap()
}
