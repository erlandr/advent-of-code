use std::fs;
use std::time::Instant;

fn main() {
    let start = Instant::now();

    let example = parse("data/example.txt");
    let input = parse("data/input.txt");

    println!("Advent of Code 2024 - Day 13");
    println!();
    println!("Example Part 1: {}", part1(&example));
    println!("Example Part 2: {}", part2(&example));
    println!();
    println!("Part 1: {}", part1(&input));
    println!("Part 2: {}", part2(&input));
    println!();

    let duration = start.elapsed();
    println!("Time elapsed: {:?}", duration);
}

#[derive(Debug)]
struct ClawMachine {
    prize: (i64, i64),
    button_a: (i64, i64),
    button_b: (i64, i64),
}

impl ClawMachine {
    fn get_uv(&self, prize_extra_distance: i64) -> (i64, i64) {
        // here we are solving a problem of intersecting rays
        // u and v represent the distance from each origin to the intersection point
        // this happens to be the same as the number of button presses for each button
        let dx = (self.prize.0 + prize_extra_distance) as f64;
        let dy = (self.prize.1 + prize_extra_distance) as f64;
        let ax = self.button_a.0 as f64;
        let ay = self.button_a.1 as f64;
        let bx = -self.button_b.0 as f64;
        let by = -self.button_b.1 as f64;
        let det = bx * ay - by * ax;
        let u = (dy * bx - dx * by) / det;
        let v = (dy * ax - dx * ay) / det;
        // check if u and v are whole numbers
        // set to 0 if they are not (no solution)
        if u.fract() != 0.0 || v.fract() != 0.0 {
            return (0, 0);
        }
        (u as i64, v as i64)
    }
}

fn parse_line(line: &str, prefix: &str) -> Vec<i64> {
    line.split(": ").collect::<Vec<&str>>()[1]
        .replace(prefix, "")
        .replace("X", "")
        .replace("Y", "")
        .split(", ")
        .map(|x| x.parse::<i64>().unwrap())
        .collect()
}

fn parse_chunk(chunk: &str) -> ClawMachine {
    let lines: Vec<&str> = chunk.trim().split('\n').collect();
    let button_a = parse_line(lines[0], "+");
    let button_b = parse_line(lines[1], "+");
    let prize = parse_line(lines[2], "=");

    ClawMachine {
        prize: (prize[0], prize[1]),
        button_a: (button_a[0], button_a[1]),
        button_b: (button_b[0], button_b[1]),
    }
}

fn parse(data_path: &str) -> Vec<ClawMachine> {
    let data = fs::read_to_string(data_path).unwrap().replace("\r", "");
    let chunks: Vec<&str> = data.split("\n\n").collect();
    chunks.iter().map(|&chunk| parse_chunk(chunk)).collect()
}

fn part1(data: &Vec<ClawMachine>) -> i64 {
    let mut cost = 0;
    for claw_machine in data {
        let (u, v) = claw_machine.get_uv(0);
        cost += u * 3;
        cost += v;
    }
    cost
}

fn part2(data: &Vec<ClawMachine>) -> i64 {
    let mut cost = 0;
    for claw_machine in data {
        let (u, v) = claw_machine.get_uv(10000000000000);
        cost += u * 3;
        cost += v;
    }
    cost
}
