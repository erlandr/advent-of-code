use std::fs;
use std::time::Instant;

fn main() {
    let start = Instant::now();

    let example_map = parse("data/example.txt");
    let input_map = parse("data/input.txt");

    println!("Advent of Code 2024 - Day 10");
    println!();
    println!("Example Part 1: {}", part1(&example_map));
    println!("Example Part 2: {}", part2(&example_map));
    println!();
    println!("Part 1: {}", part1(&input_map));
    println!("Part 2: {}", part2(&input_map));
    println!();

    let duration = start.elapsed();
    println!("Time elapsed: {:?}", duration);
}

fn parse(data_path: &str) -> Vec<Vec<usize>> {
    let data = fs::read_to_string(data_path).unwrap();
    let map: Vec<Vec<usize>> = data.trim().lines().map(|line| line.chars().map(|x| x.to_digit(10).unwrap() as usize).collect()).collect();
    return map;
}

fn find_trail_heads(map: &Vec<Vec<usize>>) -> Vec<(usize, usize)> {
    let mut starts: Vec<(usize,usize)> = Vec::new();
    for y in 0..map.len() {
        for x in 0..map[y].len() {
            if map[y][x] == 0 {
                starts.push((x,y));
            }
        }
    }
    starts
}

fn find_peaks(pos: &(usize, usize), target: usize, map: &Vec<Vec<usize>>) -> Vec<(usize, usize)> {
    if map[pos.1][pos.0] != target {
        return Vec::new();
    }
    if target == 9 {
        return vec![*pos];
    }
    let mut peaks: Vec<(usize, usize)> = Vec::new();
    if pos.1 > 0 { // up
        peaks.extend(find_peaks(&(pos.0, pos.1-1), target+1, map));
    }
    if pos.0 > 0 { // left
        peaks.extend(find_peaks(&(pos.0-1, pos.1), target+1, map));
    }
    if pos.1 < map.len()-1 { // down
        peaks.extend(find_peaks(&(pos.0, pos.1+1), target+1, map));
    }
    if pos.0 < map[pos.1].len()-1 { // right
        peaks.extend(find_peaks(&(pos.0+1, pos.1), target+1, map));
    }
    peaks
}

fn part1(map: &Vec<Vec<usize>>) -> u64 {
    let trail_heads = find_trail_heads(map);
    let mut scores = 0;
    for trail in trail_heads {
        let peaks = find_peaks(&trail, 0, map);
        // get unique peaks
        let mut unique_peaks: Vec<(usize, usize)> = Vec::new();
        for peak in peaks {
            if !unique_peaks.contains(&peak) {
                unique_peaks.push(peak);
            }
        }
        scores += unique_peaks.len() as u64;
    }
    scores
}

fn part2(map: &Vec<Vec<usize>>) -> u64 {
    let trail_heads = find_trail_heads(map);
    let mut scores = 0;
    for trail in trail_heads {
        let peaks = find_peaks(&trail, 0, map);
        scores += peaks.len() as u64;
    }
    scores
}
