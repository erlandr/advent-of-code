import time

def parse_input(data_line):
    # Convert one line of text into a test value and a list of numbers
    test_value, numbers = data_line.split(": ")
    test_value = int(test_value)
    numbers = list(map(int, numbers.split(" ")))
    return test_value, numbers

def add(x, y):
    # Reverse operation
    return x - y

def multiply(x, y):
    # Check if reverse operation is possible and return result
    if y == 0 or x % y != 0:
        return None
    return x // y

def concatenate(x, y):
    # Check if n last digits of x match y, where n is length of y
    y_str = str(y)
    x_str = str(x)
    if x_str.endswith(y_str):
        new_str = x_str[:-len(y_str)]
        if new_str and new_str != '-':  # Ensure new_str is not empty and not just '-'
            return int(new_str)
    return None

def solve(data, operators):
    # Breadth First Search approach to determine if test value is possible
    # Now in reverse!
    sum_result = 0
    for line in data.splitlines():
        test_value, numbers = parse_input(line)
        stack = [(test_value, len(numbers) - 1)]
        while stack:
            target, i = stack.pop()
            num = numbers[i]
            if i == 0:
                if num == target:
                    sum_result += test_value
                    break
                continue
            for operator in operators:
                new_value = operator(target, num)
                if new_value is not None:
                    stack.append((new_value, i - 1))
    return sum_result

def part1(data):
    return solve(data, [add, multiply])

def part2(data):
    return solve(data, [add, multiply, concatenate])

if __name__ == "__main__":
    start = time.time()

    with open("data/example.txt") as f:
        example_data = f.read()
    with open("data/input.txt") as f:
        input_data = f.read()

    print("Advent of Code 2024 - Day 7")
    print()
    print("Example Part 1:", part1(example_data))
    print("Example Part 2:", part2(example_data))
    print()
    print("Part 1:", part1(input_data))
    print("Part 2:", part2(input_data))
    print()

    duration = time.time() - start
    print("Time elapsed:", duration)
    # print time elapsed in milliseconds
    print("Time elapsed:", duration * 1000, "ms")