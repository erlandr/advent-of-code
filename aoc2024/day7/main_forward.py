import time

def parse_input(data_line):
    # Convert one line of text into a test value and a list of numbers
    test_value, numbers = data_line.split(": ")
    test_value = int(test_value)
    numbers = list(map(int, numbers.split(" ")))
    return test_value, numbers

def multiply(x, y):
    return x * y

def add(x, y):
    return x + y

def concatenate(x, y):
    return int(f"{x}{y}")

def solve(data, operators):
    # Breadth First Search approach to determine if test value is possible
    sum_result = 0
    for line in data.splitlines():
        test_value, numbers = parse_input(line)
        stack = [(numbers[0], 0)]
        while stack:
            result, i = stack.pop()
            i += 1
            if i == len(numbers):
                if result == test_value:
                    sum_result += test_value
                    break
                continue
            for operator in operators:
                new_value = operator(result, numbers[i])
                if new_value <= test_value:
                    stack.append((new_value, i))
    return sum_result

def part1(data):
    return solve(data, [add, multiply])

def part2(data):
    return solve(data, [add, multiply, concatenate])

if __name__ == "__main__":
    start = time.time()

    with open("data/example.txt") as f:
        example_data = f.read()
    with open("data/input.txt") as f:
        input_data = f.read()

    print("Advent of Code 2024 - Day 7")
    print()
    print("Example Part 1:", part1(example_data))
    print("Example Part 2:", part2(example_data))
    print()
    print("Part 1:", part1(input_data))
    print("Part 2:", part2(input_data))
    print()

    duration = time.time() - start
    print("Time elapsed:", duration)
    # print time elapsed in milliseconds
    print("Time elapsed:", duration * 1000, "ms")