use std::fs;
use std::time::Instant;

fn main() {
    let start = Instant::now();

    let example_data = fs::read_to_string("data/example.txt").unwrap();
    let input_data = fs::read_to_string("data/input.txt").unwrap();

    println!("Advent of Code 2024 - Day 7");
    println!();
    println!("Example Part 1: {}", part1(&example_data));
    println!("Example Part 2: {}", part2(&example_data));
    println!();
    println!("Part 1: {}", part1(&input_data));
    println!("Part 2: {}", part2(&input_data));
    println!();

    let duration = start.elapsed();
    println!("Time elapsed: {:?}", duration);
}

fn parse_input(data_line: &str) -> (i64, Vec<i64>) {
    // convert one line of text into a test value and a list of numbers
    let (test_value, numbers) = data_line.split_once(": ").unwrap();
    let test_value: i64 = test_value.parse().expect(&data_line);
    let numbers: Vec<i64> = numbers.split(" ").map(|x| x.parse().unwrap()).collect();
    (test_value, numbers)
}

fn add(x: i64, y: i64) -> Option<i64> {
    // Reverse operation
    Some(x - y)
}

fn multiply(x: i64, y: i64) -> Option<i64> {
    // Check if reverse operation is possible and return result
    if y == 0 || x % y != 0 {
        return None;
    }
    return Some(x / y);
}

fn concatenate(x: i64, y: i64) -> Option<i64> {
    // Check if n last digits of x match y, where n is length of y
    if x % 10i64.pow(y.to_string().len() as u32) == y {
        return Some(x / 10i64.pow(y.to_string().len() as u32));
    }
    return None;
}

fn solve(data: &str, operators: Vec<fn(i64, i64) -> Option<i64>>) -> i64 {
    // Breadth First Search approach to determine if test value is possible
    // Now in reverse!
    let mut sum = 0;
    for line in data.lines() {
        let (test_value, numbers) = parse_input(line);
        let mut stack = vec![(test_value, numbers.len() - 1)];
        while let Some((target, i)) = stack.pop() {
            let num = numbers[i];
            if i == 0 {
                if num == target {
                    sum += test_value;
                    break;
                }
                continue;
            }
            for operator in &operators {
                if let Some(new_value) = operator(target, num) {
                    stack.push((new_value, i - 1));
                }
            }
        }
    }
    sum
}

fn part1(data: &str) -> i64 {
    solve(data, vec![add, multiply])
}

fn part2(data: &str) -> i64 {
    solve(data, vec![add, multiply, concatenate])
}
