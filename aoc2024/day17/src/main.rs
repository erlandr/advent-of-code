use std::fs;
use std::time::Instant;

fn main() {
    let start = Instant::now();

    println!("Advent of Code 2024 - Day 17");
    println!();
    println!("Example Part 1: {:?}", part1("data/example.txt"));
    println!("Example Part 2: {}", part2("data/example2.txt"));
    println!();
    println!("Part 1: {:?}", part1("data/input.txt"));
    println!("Part 2: {}", part2("data/input.txt"));
    println!();

    let duration = start.elapsed();
    println!("Time elapsed: {:?}", duration);
}

#[derive(Debug)]
struct Computer {
    register_a: i64,
    register_b: i64,
    register_c: i64,
    program: Vec<usize>,
    instruction_pointer: usize,
    output: Vec<i64>,
}

impl Computer {
    fn new(register_a: i64, register_b: i64, register_c: i64, program: Vec<usize>) -> Computer {
        Computer {
            register_a,
            register_b,
            register_c,
            program,
            instruction_pointer: 0,
            output: Vec::new(),
        }
    }

    fn reset(&mut self) {
        self.register_a = 0;
        self.register_b = 0;
        self.register_c = 0;
        self.instruction_pointer = 0;
        self.output = Vec::new();
    }

    fn run_program(&mut self) {
        loop {
            let opcode = self.program[self.instruction_pointer];
            let operand = self.program[self.instruction_pointer + 1] as i64;
            self.run_opcode(opcode, operand);
            if self.instruction_pointer >= self.program.len() {
                break;
            }
        }
    }

    fn run_opcode(&mut self, opcode: usize, operand: i64) {
        match opcode {
            0 => self.adv(operand),
            1 => self.bxl(operand),
            2 => self.bst(operand),
            3 => self.jnz(operand),
            4 => self.bxc(operand),
            5 => self.out(operand),
            6 => self.bdv(operand),
            7 => self.cdv(operand),
            _ => panic!("Unknown opcode: {}", opcode),
        }
    }

    fn combo_operand(&self, operand: i64) -> i64 {
        match operand {
            0..=3 => operand,
            4 => self.register_a,
            5 => self.register_b,
            6 => self.register_c,
            _ => panic!("Unknown operand: {}", operand),
        }
    }

    fn adv(&mut self, operand: i64) {
        // division of register A by 2 raised to the power of the literal value
        let numerator = self.register_a;
        let denominator = 2_i64.pow(self.combo_operand(operand) as u32);
        self.register_a = numerator / denominator;
        self.instruction_pointer += 2;
    }

    fn bxl(&mut self, operand: i64) {
        // bitwise XOR of register B and the literal value
        self.register_b = self.register_b ^ operand;
        self.instruction_pointer += 2;
    }

    fn bst(&mut self, operand: i64) {
        // combo operand modulo 8
        self.register_b = self.combo_operand(operand) % 8;
        self.instruction_pointer += 2;
    }

    fn jnz(&mut self, operand: i64) {
        // jump to the instruction at the literal operand if A is not 0
        if self.register_a != 0 {
            self.instruction_pointer = operand as usize;
        } else {
            self.instruction_pointer += 2;
        }
    }

    fn bxc(&mut self, _operand: i64) {
        // bitwise XOR of register B and register C
        self.register_b = self.register_b ^ self.register_c;
        self.instruction_pointer += 2;
    }

    fn out(&mut self, operand: i64) {
        // output combo operand modulo 8
        let operand = self.combo_operand(operand) % 8;
        self.output.push(operand);
        self.instruction_pointer += 2;
    }

    fn bdv(&mut self, operand: i64) {
        // division of register A by 2 raised to the power of the combo operand
        let numerator = self.register_a;
        let denominator = 2_i64.pow(self.combo_operand(operand) as u32);
        self.register_b = numerator / denominator;
        self.instruction_pointer += 2;
    }

    fn cdv(&mut self, operand: i64) {
        // division of register C by 2 raised to the power of the combo operand
        let numerator = self.register_a;
        let denominator = 2_i64.pow(self.combo_operand(operand) as u32);
        self.register_c = numerator / denominator;
        self.instruction_pointer += 2;
    }

    fn output_string(&self) -> String {
        self.output.iter().map(|x| x.to_string()).collect::<Vec<String>>().join(",")
    }
}

fn parse(path: &str) -> Computer {
    let contents = fs::read_to_string(path).unwrap();
    let lines: Vec<&str> = contents.lines().collect();
    let register_a = lines[0].split_once(": ").unwrap().1.parse::<i64>().unwrap();
    let register_b = lines[1].split_once(": ").unwrap().1.parse::<i64>().unwrap();
    let register_c = lines[2].split_once(": ").unwrap().1.parse::<i64>().unwrap();
    let program = lines[4]
        .split_once(": ")
        .unwrap().1
        .split(",")
        .map(|x| x.trim().parse::<usize>().expect(&(x.to_string() + " is not a number")))
        .collect::<Vec<usize>>();
    Computer::new(register_a, register_b, register_c, program)
}

fn part1(path: &str) -> String {
    let mut computer = parse(path);
    computer.run_program();
    computer.output_string()
}

fn part2(path: &str) -> i64 {
    let mut computer = parse(path);
    let mut target_a = 0;
    for i in 0..computer.program.len() {
        let end = if i == computer.program.len() - 1 { 1000 } else { 8 };
        for a in 0..end {
            computer.reset();
            computer.register_a = (target_a << 3) | a;
            computer.run_program();
            let out = &computer.output;
            let prog = computer.program[computer.program.len() - 1 - i..]
                .iter()
                .map(|&x| x as i64)
                .collect::<Vec<i64>>();
            if *out == prog {
                target_a = (target_a << 3) | a;
                break;
            }
        }
    }
    target_a
}
