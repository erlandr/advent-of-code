use std::collections::HashMap;
use std::fs;
use std::time::Instant;

fn main() {
    let start = Instant::now();

    println!("Advent of Code 2024 - Day 19");
    println!();
    println!("Example Part 1: {}", part1("data/example.txt"));
    println!("Example Part 2: {}", part2("data/example.txt"));
    println!();
    println!("Part 1: {}", part1("data/input.txt"));
    println!("Part 2: {}", part2("data/input.txt"));
    println!();

    let duration = start.elapsed();
    println!("Time elapsed: {:?}", duration);
}

fn parse(path: &str) -> (Vec<String>, Vec<String>) {
    let content = fs::read_to_string(path).unwrap();
    let data: Vec<&str> = content.lines().collect();
    let patterns: Vec<String> = data[0]
        .split(", ")
        .map(|pattern| pattern.to_string())
        .collect();
    let designs = data[2..].iter().map(|s| s.to_string()).collect();
    (patterns, designs)
}

fn get_patterns_subset(patterns: &Vec<String>, design: &str) -> Vec<String> {
    let mut patterns_subset: Vec<String> = Vec::new();
    for a in patterns {
        if design.contains(a) {
            patterns_subset.push(a.to_string());
        }
    }
    patterns_subset
}

fn design_permutations(
    design: &str,
    patterns: &Vec<String>,
    memo: &mut HashMap<String, usize>,
) -> usize {
    if design.is_empty() {
        return 1;
    }
    if let Some(&permutations) = memo.get(design) {
        return permutations;
    }
    let mut permutations = 0;
    for p in patterns.iter() {
        if design.starts_with(p) {
            let remaining_design = design[p.len()..].to_string();
            permutations += design_permutations(&remaining_design, &patterns, memo);
        }
    }
    memo.insert(design.to_string(), permutations);
    permutations
}

fn part1(path: &str) -> usize {
    let (patterns, designs) = parse(path);
    let mut memo: HashMap<String, usize> = HashMap::new();
    designs
        .iter()
        .map(|d| {
            let patterns = get_patterns_subset(&patterns, d);
            (design_permutations(d, &patterns, &mut memo) > 0) as usize
        })
        .sum()
}

fn part2(path: &str) -> usize {
    let (patterns, designs) = parse(path);
    let mut memo: HashMap<String, usize> = HashMap::new();
    designs
        .iter()
        .map(|d| {
            let patterns = get_patterns_subset(&patterns, d);
            design_permutations(d, &patterns, &mut memo)
        })
        .sum()
}
