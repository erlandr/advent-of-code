use std::fs;
use std::time::Instant;
use std::collections::HashMap;
use std::collections::HashSet;

fn main() {
    let start = Instant::now();

    println!("Advent of Code 2024 - Day 16");
    println!();
    println!("Example Part 1: {}", part1("data/example.txt"));
    println!("Example Part 1: {}", part1("data/example2.txt"));
    println!("Example Part 2: {}", part2("data/example.txt"));
    println!("Example Part 2: {}", part2("data/example2.txt"));
    println!();
    println!("Part 1: {}", part1("data/input.txt"));
    println!("Part 2: {}", part2("data/input.txt"));
    println!();

    let duration = start.elapsed();
    println!("Time elapsed: {:?}", duration);
}

#[derive(PartialEq, Eq, Hash, Clone)]
enum Dir {
    North,
    South,
    West,
    East,
}

impl Dir {
    fn iter() -> impl Iterator<Item = Dir> {
        [Dir::North, Dir::South, Dir::West, Dir::East].iter().cloned()
    }
}

// dir lookup to x, y
impl From<Dir> for (i32, i32) {
    fn from(dir: Dir) -> Self {
        match dir {
            Dir::North => (0, -1),
            Dir::South => (0, 1),
            Dir::West => (-1, 0),
            Dir::East => (1, 0),
        }
    }
}

#[derive(PartialEq, Eq)]
enum Tile {
    Empty,
    Wall,
    Start,
    End
}

struct Map {
    grid: Vec<Vec<Tile>>,
    start: (usize, usize),
    end: (usize, usize),
    visited: HashMap<(usize, usize, Dir), (usize, usize)>,
}

impl Map {
    fn new(grid: Vec<Vec<Tile>>, start: (usize, usize), end: (usize, usize)) -> Self {
        Map {
            grid,
            start,
            end,
            visited: HashMap::new(),
        }
    }

    fn shortest_path(&mut self) -> usize {
        let mut check_pos: Vec<(usize, usize, Dir, usize)> = vec![(self.start.0, self.start.1, Dir::East, 0)];
        let mut et: usize = 0;
        while check_pos.len() > 0 {
            let mut new_check_pos: Vec<(usize, usize, Dir, usize)> = Vec::new();
            while let Some((x, y, dir, turns)) = check_pos.pop() {
                if self.grid[y][x] != Tile::Wall {
                    let space_time;
                    let space_turns;
                    match self.visited.get(&(x, y, dir.clone())) {
                        Some((t, tns)) => {
                            space_time = *t;
                            space_turns = *tns;
                        },
                        None => {
                            space_time = et + 1;
                            space_turns = turns + 1;
                        },
                    }
                    let space_total = (space_turns * 1000) + space_time;
                    if space_total > (turns * 1000) + et {
                        self.visited.insert((x, y, dir.clone()), (et, turns));
                        if self.grid[y][x] == Tile::End {
                            continue;
                        }
                        if x < self.grid[0].len() - 1 {
                            if dir == Dir::East {
                                new_check_pos.push((x + 1, y, Dir::East, turns));
                            } else if dir != Dir::West {
                                new_check_pos.push((x + 1, y, Dir::East, turns + 1));
                            }
                        }
                        if x > 0 {
                            if dir == Dir::West {
                                new_check_pos.push((x - 1, y, Dir::West, turns));
                            } else if dir != Dir::East {
                                new_check_pos.push((x - 1, y, Dir::West, turns + 1));
                            }
                        }
                        if y < self.grid.len() - 1 {
                            if dir == Dir::South {
                                new_check_pos.push((x, y + 1, Dir::South, turns));
                            } else if dir != Dir::North {
                                new_check_pos.push((x, y + 1, Dir::South, turns + 1));
                            }
                        }
                        if y > 0 {
                            if dir == Dir::North {
                                new_check_pos.push((x, y - 1, Dir::North, turns));
                            } else if dir != Dir::South {
                                new_check_pos.push((x, y - 1, Dir::North, turns + 1));
                            }
                        }
                    }
                }
            }
            check_pos = new_check_pos;
            et += 1;
        }
        // try getting end time for each direction
        let mut lowest_score = usize::MAX;
        for d in Dir::iter() {
            if let Some((t, tns)) = self.visited.get(&(self.end.0, self.end.1, d)) {
                if (*tns * 1000) + *t < lowest_score {
                    lowest_score = (*tns * 1000) + *t;
                }
            }
        }
        lowest_score
    }

    fn find_fastest_paths(&mut self) -> usize {
        let shortest_time = self.shortest_path();
        let mut check_pos: Vec<(usize, usize, Dir, usize, usize)> = Vec::new();
        let mut check_time: usize = 0;
        let mut path_tiles: HashSet<(usize, usize)> = HashSet::new();

        for d in Dir::iter() {
            if let Some((t, tns)) = self.visited.get(&(self.end.0, self.end.1, d.clone())) {
                if (*tns * 1000) + *t == shortest_time {
                    check_pos.push((self.end.0, self.end.1, d.clone(), *tns, *t));
                    check_time = *t;
                }
            }
        }

        while check_pos.len() > 0 {
            let mut new_check_pos: Vec<(usize, usize, Dir, usize, usize)> = Vec::new();
            while let Some((x, y, dir, turns, time)) = check_pos.pop() {
                if check_time == time {
                    path_tiles.insert((x, y));
                    if (x, y) == self.start {
                        continue;
                    }
                    let (dx, dy): (i32, i32) = dir.into();
                    let prev_x = (x as i32 - dx) as usize;
                    let prev_y = (y as i32 - dy) as usize;
                    for d in Dir::iter() {
                        if let Some((t, tns)) = self.visited.get(&(prev_x, prev_y, d.clone())) {
                            if *tns <= turns {
                                new_check_pos.push((prev_x, prev_y, d.clone(), *tns, *t));
                            }
                        }
                    }
                }
            }
            if check_time == 0 {
                break;
            }
            check_pos = new_check_pos;
            check_time -= 1;
        }
        path_tiles.len()
    }
        
}

fn parse_map(path: &str) -> Map {
    let mut start = (0, 0);
    let mut end = (0, 0);
    let mut grid: Vec<Vec<Tile>> = Vec::new();

    for (y, line) in fs::read_to_string(path).unwrap().lines().enumerate() {
        let row: Vec<Tile> = line
            .chars()
            .enumerate()
            .map(|(x, c)| match c {
                '#' => Tile::Wall,
                '.' => Tile::Empty,
                'S' => {
                    start = (x, y);
                    Tile::Start
                }
                'E' => {
                    end = (x, y);
                    Tile::End
                }
                _ => panic!("Invalid character in map"),
            })
            .collect();
        grid.push(row);
    }

    Map ::new(grid, start, end)
}

fn part1(path: &str) -> usize {
    let mut map = parse_map(path);
    map.shortest_path()

}

fn part2(path: &str) -> usize {
    // 595 too high
    let mut map = parse_map(path);
    map.find_fastest_paths()
}
