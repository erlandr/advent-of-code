use std::fs;
use regex::Regex;
use std::time::Instant;
use lazy_static::lazy_static;

lazy_static! {
    static ref RE_MUL: Regex = Regex::new(r"mul\((\d+),(\d+)\)").unwrap();
    static ref RE_COMMAND: Regex = Regex::new(r"mul\((\d+),(\d+)\)|do\(\)|don't\(\)").unwrap();
}

fn main() {
    let start = Instant::now();

    let example_data = fs::read_to_string("data/example.txt").unwrap();
    let input_data = fs::read_to_string("data/input.txt").unwrap();

    println!("Advent of Code 2024 - Day 3");
    println!();
    println!("Example Part 1: {}", part1(&example_data));
    println!("Example Part 2: {}", part2(&example_data));
    println!();
    println!("Part 1: {}", part1(&input_data));
    println!("Part 2: {}", part2(&input_data));
    println!();

    let duration = start.elapsed();
    println!("Time elapsed: {:?}", duration);
}

fn part1(data: &str) -> i32 {
    let mut sum = 0;
    for cap in RE_MUL.captures_iter(&data) {
        let x: i32 = cap[1].parse().unwrap();
        let y: i32 = cap[2].parse().unwrap();
        sum += x * y;
    }
    sum
}

fn part2(data: &str) -> i32 {
    let mut sum = 0;
    let mut enabled= true;
    for cap in RE_COMMAND.captures_iter(&data) {
        match &cap[0] {
            "do()" => enabled = true,
            "don't()" => enabled = false,
            _ => {
                if enabled {
                    let x: i32 = cap[1].parse().unwrap();
                    let y: i32 = cap[2].parse().unwrap();
                    sum += x * y;
                }
            }
        }
    }
    sum
}
