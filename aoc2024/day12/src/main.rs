use std::collections::HashMap;
use std::fs;
use std::time::Instant;

fn main() {
    let start = Instant::now();

    let example = parse("data/example.txt");
    let input = parse("data/input.txt");

    println!("Advent of Code 2024 - Day 12");
    println!();
    println!("Example Part 1: {}", part1(&example));
    println!("Example Part 2: {}", part2(&example));
    println!();
    println!("Part 1: {}", part1(&input));
    println!("Part 2: {}", part2(&input));
    println!();

    let duration = start.elapsed();
    println!("Time elapsed: {:?}", duration);
}

fn parse(data_path: &str) -> Vec<Vec<char>> {
    let data = fs::read_to_string(data_path).unwrap();
    let data = data.lines().map(|line| line.chars().collect()).collect();
    data
}

// (x, y, is_right/is_down)
type Border = (usize, usize, bool);

fn explore(
    data: &Vec<Vec<char>>,
    pos: (usize, usize),
    dir: (i32, i32),
    target_label: char,
    region: u32,
    plots: &mut HashMap<(usize, usize), u32>,
) -> (u64, (Vec<Border>, Vec<Border>)) {
    // Find all plots in the region, return (area, (vertical borders, horizontal borders))
    // set x and y to pos + dir
    let x = (pos.0 as i32 + dir.0) as usize;
    let y = (pos.1 as i32 + dir.1) as usize;
    let label = data[y][x];
    if label != target_label {
        if dir.0 > 0 {
            // crossed vertical border (moving right)
            return (0, (vec![(x, y, true)], vec![]));
        } else if dir.0 < 0 {
            // crossed vertical border (moving left)
            return (0, (vec![(pos.0, pos.1, false)], vec![]));
        } else if dir.1 > 0 {
            // crossed horizontal border (moving down)
            return (0, (vec![], vec![(x, y, true)]));
        } else {
            // crossed horizontal border (moving up)
            return (0, (vec![], vec![(pos.0, pos.1, false)]));
        }
    }
    if plots.contains_key(&(x, y)) {
        // already visited
        return (0, (vec![], vec![]));
    }
    plots.insert((x, y), region);

    let mut area = 1;
    let mut vertical_borders = Vec::new();
    let mut horizontal_borders = Vec::new();
    // Down
    if y < data.len() - 1 {
        let (a, (vb, hb)) = explore(data, (x, y), (0, 1), target_label, region, plots);
        area += a;
        vertical_borders.extend(vb);
        horizontal_borders.extend(hb);
    } else {
        horizontal_borders.push((x, y + 1, true));
    }
    // Up
    if y > 0 {
        let (a, (vb, hb)) = explore(data, (x, y), (0, -1), target_label, region, plots);
        area += a;
        vertical_borders.extend(vb);
        horizontal_borders.extend(hb);
    } else {
        horizontal_borders.push((x, y, false));
    }
    // Right
    if x < data[y].len() - 1 {
        let (a, (vb, hb)) = explore(data, (x, y), (1, 0), target_label, region, plots);
        area += a;
        vertical_borders.extend(vb);
        horizontal_borders.extend(hb);
    } else {
        vertical_borders.push((x + 1, y, true));
    }
    // Left
    if x > 0 {
        let (a, (vb, hb)) = explore(data, (x, y), (-1, 0), target_label, region, plots);
        area += a;
        vertical_borders.extend(vb);
        horizontal_borders.extend(hb);
    } else {
        vertical_borders.push((x, y, false));
    }

    (area, (vertical_borders, horizontal_borders))
}

fn count_sides(
    vertical_borders: &Vec<Border>,
    horizontal_borders: &Vec<Border>,
) -> u64 {
    // Vertical
    let mut v_sides = 1;
    let mut vertical_borders = vertical_borders.clone();
    // sort by x, then y
    vertical_borders.sort_by(|a, b| a.0.cmp(&b.0).then(a.1.cmp(&b.1)));
    let mut prev = vertical_borders[0];
    for i in 1..vertical_borders.len() {
        let (x, y, is_left) = vertical_borders[i];
        if x != prev.0 {
            v_sides += 1;
        } else if y != prev.1 + 1 {
            v_sides += 1;
        } else if is_left != prev.2 {
            v_sides += 1;
        }
        prev = (x, y, is_left);
    }
    // Horizontal
    let mut h_sides = 1;
    let mut horizontal_borders = horizontal_borders.clone();
    // sort by y, then x
    horizontal_borders.sort_by(|a, b| a.1.cmp(&b.1).then(a.0.cmp(&b.0)));
    prev = horizontal_borders[0];
    for i in 1..horizontal_borders.len() {
        let (x, y, is_down) = horizontal_borders[i];
        if y != prev.1 {
            h_sides += 1;
        } else if x != prev.0 + 1 {
            h_sides += 1;
        } else if is_down != prev.2 {
            h_sides += 1;
        }
        prev = (x, y, is_down);
    }
    v_sides + h_sides
}

fn solve(data: &Vec<Vec<char>>, discount: bool) -> u64 {
    let mut plots: HashMap<(usize, usize), u32> = HashMap::new();
    let mut total_cost = 0;
    let mut region = 0;
    for y in 0..data.len() {
        for x in 0..data[y].len() {
            if plots.contains_key(&(x, y)) {
                continue;
            }
            let target_label = data[y][x];
            let (total_area, (vb, hb)) =
                explore(data, (x, y), (0, 0), target_label, region, &mut plots);
            if discount {
                let sides = count_sides(&vb, &hb);
                // println!("Region {}: {} sides", region, sides);
                total_cost += total_area * sides;
            } else {
                let borders = (vb.len() + hb.len()) as u64;
                total_cost += total_area * borders;
            }
            region += 1;
        }
    }
    total_cost
}

fn part1(data: &Vec<Vec<char>>) -> u64 {
    solve(data, false)
}

fn part2(data: &Vec<Vec<char>>) -> u64 {
    solve(data, true)
}
