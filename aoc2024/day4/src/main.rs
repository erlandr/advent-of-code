use std::fs;
use std::time::Instant;

const RIGHT: (i32, i32) = (1, 0);
const RIGHT_DOWN: (i32, i32) = (1, 1);
const DOWN: (i32, i32) = (0, 1);
const LEFT_DOWN: (i32, i32) = (-1, 1);
const LEFT: (i32, i32) = (-1, 0);
const LEFT_UP: (i32, i32) = (-1, -1);
const UP: (i32, i32) = (0, -1);
const RIGHT_UP: (i32, i32) = (1, -1);

fn main() {
    let start = Instant::now();

    let example_data = fs::read_to_string("data/example.txt").unwrap();
    let input_data = fs::read_to_string("data/input.txt").unwrap();

    println!("Advent of Code 2024 - Day 4");
    println!();
    println!("Example Part 1: {}", part1(&example_data));
    println!("Example Part 2: {}", part2(&example_data));
    println!();
    println!("Part 1: {}", part1(&input_data));
    println!("Part 2: {}", part2(&input_data));
    println!();

    let duration = start.elapsed();
    println!("Time elapsed: {:?}", duration);
}

fn part1(data: &str) -> i32 {
    let mut sum = 0;
    let data_lines: Vec<&str> = data.lines().collect();
    let width = data_lines[0].len();
    let height = data_lines.len();
    for x in 0..width {
        for y in 0..height {
            if data_lines[y].chars().nth(x).unwrap() == 'X' {
                sum += find_mas(&data_lines, x, y, RIGHT);
                sum += find_mas(&data_lines, x, y, RIGHT_DOWN);
                sum += find_mas(&data_lines, x, y, DOWN);
                sum += find_mas(&data_lines, x, y, LEFT_DOWN);
                sum += find_mas(&data_lines, x, y, LEFT);
                sum += find_mas(&data_lines, x, y, LEFT_UP);
                sum += find_mas(&data_lines, x, y, UP);
                sum += find_mas(&data_lines, x, y, RIGHT_UP);
            }
        }
    }
    sum
}

fn find_mas(data_lines: &Vec<&str>, x: usize, y: usize, direction: (i32, i32)) -> i32 {
    let mut x: i32 = x as i32;
    let mut y: i32 = y as i32;
    for letter in "MAS".chars() {
        x += direction.0;
        y += direction.1;
        if x < 0 || y < 0 || x >= data_lines[0].len() as i32 || y >= data_lines.len() as i32 {
            return 0;
        }
        if data_lines[y as usize].chars().nth(x as usize).unwrap() != letter {
            return 0;
        }
    }
    1
}

fn part2(data: &str) -> i32 {
    let mut sum = 0;
    let data_lines: Vec<&str> = data.lines().collect();
    let width = data_lines[0].len();
    let height = data_lines.len();
    for x in 1..width-1 {
        for y in 1..height-1 {
            if data_lines[y].chars().nth(x).unwrap() == 'A' {
                let ul = data_lines[y-1].chars().nth(x-1).unwrap();
                let dr = data_lines[y+1].chars().nth(x+1).unwrap();
                let word1 = ul.to_string() + "A" + &dr.to_string();
                if word1 != "MAS" && word1 != "SAM" {
                    continue;
                }
                let ur = data_lines[y-1].chars().nth(x+1).unwrap();
                let dl = data_lines[y+1].chars().nth(x-1).unwrap();
                let word2 = ur.to_string() + "A" + &dl.to_string();
                if word2 != "MAS" && word2 != "SAM" {
                    continue;
                }
                sum += 1;
            }
        }
    }
    sum
}
