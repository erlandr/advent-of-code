use std::fs;
use std::time::Instant;

fn main() {
    let start = Instant::now();

    let example = parse("data/example.txt");
    let input = parse("data/input.txt");

    println!("Advent of Code 2024 - Day 14");
    println!();
    println!("Example Part 1: {}", part1(&example, (11, 7)));
    println!();
    println!("Part 1: {}", part1(&input, (101, 103)));
    println!("Part 2: {}", part2(&input, (101, 103)));
    println!();

    let duration = start.elapsed();
    println!("Time elapsed: {:?}", duration);
}

#[derive(Debug)]
struct Robot {
    position: (i64, i64),
    velocity: (i64, i64),
}

impl Robot {
    fn move_(&self, seconds: i64, bounds: (i64, i64)) -> (i64, i64) {
        // move the robot's position by its velocity * seconds
        // wrap its position within bounds
        let mut x = (self.position.0 + self.velocity.0 * seconds) % bounds.0;
        if x < 0 {
            x = bounds.0 + x;
        }
        let mut y = (self.position.1 + self.velocity.1 * seconds) % bounds.1;
        if y < 0 {
            y = bounds.1 + y;
        }
        (x, y)
    }
}

fn parse(data_path: &str) -> Vec<Robot> {
    let data = fs::read_to_string(data_path).unwrap();
    // chunks.iter().map(|&chunk| parse_chunk(chunk)).collect()
    let robots: Vec<Robot> = data
        .lines()
        .map(|line| {
            let parts: Vec<&str> = line.split(" ").collect();
            let position: Vec<i64> = parts[0]
                .replace("p=", "")
                .split(",")
                .map(|num| num.parse().unwrap())
                .collect();
            let velocity: Vec<i64> = parts[1]
                .replace("v=", "")
                .split(",")
                .map(|num| num.parse().unwrap())
                .collect();
            Robot {
                position: (position[0], position[1]),
                velocity: (velocity[0], velocity[1]),
            }
        })
        .collect();
    robots
}

fn move_into_quads(robots: &Vec<Robot>, bounds: (i64, i64), seconds: i64) -> (i64, i64, i64, i64) {
    let mut quads: (i64, i64, i64, i64) = (0, 0, 0, 0);
    let x_half = bounds.0 / 2;
    let y_half = bounds.1 / 2;
    for i in 0..robots.len() {
        let pos = robots[i].move_(seconds, bounds);
        if pos.0 < x_half {
            if pos.1 < y_half {
                quads.0 += 1;
            }
            if pos.1 > y_half {
                quads.1 += 1;
            }
        }
        if pos.0 > x_half {
            if pos.1 < y_half {
                quads.2 += 1;
            }
            if pos.1 > y_half {
                quads.3 += 1;
            }
        }
    }
    quads
}

fn part1(robots: &Vec<Robot>, bounds: (i64, i64)) -> i64 {
    let quads = move_into_quads(robots, bounds, 100);
    quads.0 * quads.1 * quads.2 * quads.3
}

fn draw(positions: &Vec<(i64, i64)>, bounds: (i64, i64)) {
    let mut pixels = Vec::new();
    for _ in 0..bounds.1 as usize {
        pixels.push(vec![' '; bounds.0 as usize]);
    }
    for i in 0..positions.len() {
        pixels[positions[i].1 as usize][positions[i].0 as usize] = '*';
    }
    for i in 0..pixels.len() {
        let line: String = pixels[i].iter().collect();
        println!("{}", line);
    }
}

fn part2(robots: &Vec<Robot>, bounds: (i64, i64)) -> i64 {
    let mut seconds = 1;
    let center_size = bounds.0 / 4;
    let center_vol = (bounds.0 - center_size * 2) * (bounds.0 - center_size * 2);
    loop {
        let mut center = 0;
        let mut positions: Vec<(i64, i64)> = Vec::new();
        for i in 0..robots.len() {
            let pos = robots[i].move_(seconds, bounds);
            positions.push(pos);
            // Check if position is within the center area
            if pos.0 > center_size && pos.0 < bounds.0 - center_size {
                if pos.1 > center_size && pos.1 < bounds.1 - center_size {
                    center += 1;
                }
            }
        }
        if center > center_vol / 8 {
            draw(&positions, bounds);
            break;
        }
        if seconds > 10000 {
            break;
        }
        seconds += 1;
    }
    seconds
}
