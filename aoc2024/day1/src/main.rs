use std::{fs, collections::HashMap};

fn main() {
    println!("Advent of Code 2024 - Day 1");
    println!();
    println!("Example Part 1: {}", part1("data/example.txt"));
    println!("Example Part 2: {}", part2("data/example.txt"));
    println!();
    println!("Part 1: {}", part1("data/input.txt"));
    println!("Part 2: {}", part2("data/input.txt"));
    println!();
}

fn parse(data_path: &str) -> (Vec<i32>, Vec<i32>) {
    // Read data file, parse into two columns
    let data: String = fs::read_to_string(data_path).unwrap();
    let mut col1: Vec<i32> = Vec::new();
    let mut col2: Vec<i32> = Vec::new();
    for line in data.lines() {
        let cols: Vec<&str> = line.split_whitespace().collect();
        col1.push(cols[0].parse().unwrap());
        col2.push(cols[1].parse().unwrap());
    }
    (col1, col2)
}

fn part1(data_path: &str) -> i32 {
    // Sort columns, sum absolute differences
    let (mut col1, mut col2) = parse(data_path);
    col1.sort();
    col2.sort();
    let mut distance: i32 = 0;
    for (c1, c2) in col1.iter().zip(col2.iter()) {
        distance += (c1 - c2).abs();
    }
    distance
}

fn part2(data_path: &str) -> i32 {
    // Multiply column 1 by number of matches in column 2
    let (col1, col2) = parse(data_path);
    // Hash map that counts occurrences of each value in col2
    let mut col2_counts: HashMap<i32, i32> = HashMap::new();
    for &y in col2.iter() {
        *col2_counts.entry(y).or_insert(0) += 1;
    }
    // Calculate distance
    let mut distance: i32 = 0;
    for x in col1.iter() {
        if let Some(&count) = col2_counts.get(x) {
            distance += x * count;
        }
    }
    distance
}