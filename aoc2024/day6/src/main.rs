use std::fs;
use std::time::Instant;
use std::collections::HashSet;

const RIGHT: (i32, i32) = (1, 0);
const DOWN: (i32, i32) = (0, 1);
const LEFT: (i32, i32) = (-1, 0);
const UP: (i32, i32) = (0, -1);
const DIRECTIONS: [(i32, i32); 4] = [UP, RIGHT, DOWN, LEFT];

fn main() {
    let start = Instant::now();

    let example_data = fs::read_to_string("data/example.txt").unwrap();
    let input_data = fs::read_to_string("data/input.txt").unwrap();

    println!("Advent of Code 2024 - Day 6");
    println!();
    println!("Example Part 1: {}", part1(&example_data));
    println!("Example Part 2: {}", part2(&example_data));
    println!();
    println!("Part 1: {}", part1(&input_data));
    println!("Part 2: {}", part2(&input_data));
    println!();

    let duration = start.elapsed();
    println!("Time elapsed: {:?}", duration);
}

fn get_start_pos(map_lines: &Vec<&str>) -> (i32, i32) {
    let start_pos: (i32, i32) = map_lines.iter()
        .enumerate()
        .flat_map(|(y, line)| line.chars().enumerate().map(move |(x, c)| (x, y, c)))
        .find(|&(_, _, c)| c == '^')
        .map(|(x, y, _)| (x as i32, y as i32))
        .unwrap_or((0, 0));
    start_pos
}

fn is_out_of_bounds(pos: (i32, i32), width: i32, height: i32) -> bool {
    pos.0 < 0 || pos.0 >= width || pos.1 < 0 || pos.1 >= height
}

fn is_blocked(pos: (i32, i32), map_lines: &Vec<&str>, extra_obstruction: (i32, i32)) -> bool {
    if pos == extra_obstruction {
        return true;
    }
    let pos_str: String = pos.0.to_string() + ", " + &pos.1.to_string();
    map_lines[pos.1 as usize].chars().nth(pos.0 as usize).expect(&pos_str) == '#'
}

fn get_visited(data: &str, extra_obstruction: (i32, i32)) -> (HashSet<(i32, i32)>, bool) {
    // Return visited positions and if the path is a loop
    let mut visited:HashSet<(i32, i32)>  = HashSet::new();      // unique positions
    let mut path:HashSet<(i32, i32, usize)>  = HashSet::new();  // unique position + directions
    let map_lines = data.lines().collect::<Vec<&str>>();
    let mut pos: (i32, i32) = get_start_pos(&map_lines);
    let mut mut_dir_idx = 0;
    let width = map_lines[0].len() as i32;
    let height = map_lines.len() as i32;
    'main: loop {
        // Look for loops by checking path
        let before_len = path.len();
        visited.insert(pos);
        path.insert((pos.0, pos.1, mut_dir_idx));
        if path.len() == before_len {
            return (visited, true);
        }
        loop {
            let next_pos = (pos.0 + DIRECTIONS[mut_dir_idx].0, pos.1 + DIRECTIONS[mut_dir_idx].1);
            if is_out_of_bounds(next_pos, width, height) {
                break 'main;
            } else if !is_blocked(next_pos, &map_lines, extra_obstruction) {
                pos = next_pos;
                break;
            } else {
                mut_dir_idx = (mut_dir_idx + 1) % 4;
            }
        }
    }
    (visited, false)
}

fn part1(data: &str) -> i32 {
    let (visited, _loop) = get_visited(data, (-1, -1));
    visited.len() as i32
}

fn part2(data: &str) -> i32 {
    let (visited, _loop) = get_visited(data, (-1, -1));
    visited.iter().map(|&vis_pos| {
        let (_, has_loop) = get_visited(data, vis_pos);
        if has_loop {
            1
        } else {
            0
        }
    }).sum::<i32>() - 1 // Remove the start position
}
