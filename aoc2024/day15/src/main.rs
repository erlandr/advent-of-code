use std::fs;
use std::time::Instant;
use std::collections::HashSet;

fn main() {
    let start = Instant::now();

    println!("Advent of Code 2024 - Day 15");
    println!();
    println!("Example Part 1: {}", part1("data/example.txt"));
    println!("Example Part 2: {}", part2("data/example.txt"));
    println!();
    println!("Part 1: {}", part1("data/input.txt"));
    println!("Part 2: {}", part2("data/input.txt"));
    println!();

    let duration = start.elapsed();
    println!("Time elapsed: {:?}", duration);
}

#[derive(PartialEq, Eq, Clone)]
enum Tile {
    Empty,
    Box,
    Wall,
    BoxLeft,
    BoxRight,
}

struct Warehouse {
    map: Vec<Vec<Tile>>, 
    robot_position: (usize, usize),
}

impl  Warehouse {
    fn move_robot(&mut self, direction: (i8, i8)) {
        let x = (self.robot_position.0 as i8 + direction.0) as usize;
        let y = (self.robot_position.1 as i8 + direction.1) as usize;
        if self.map[y][x] == Tile::Box {
            self.push_box((x, y), direction);
        }
        if self.map[y][x] == Tile::BoxLeft || self.map[y][x] == Tile::BoxRight {
            self.push_thick_box((x, y), direction);
        }
        if self.map[y][x] == Tile::Empty {
            self.robot_position.0 = x as usize;
            self.robot_position.1 = y as usize;
        }
    }

    fn push_box(&mut self, pos: (usize, usize), direction: (i8, i8)) {
        let mut new_pos = pos;
        while self.map[new_pos.1][new_pos.0] == Tile::Box {
            new_pos.0 = (new_pos.0 as i8 + direction.0) as usize;
            new_pos.1 = (new_pos.1 as i8 + direction.1) as usize;
        }
        if self.map[new_pos.1][new_pos.0] == Tile::Empty {
            self.map[new_pos.1][new_pos.0] = Tile::Box;
            self.map[pos.1][pos.0] = Tile::Empty;
        }
    }

    fn push_thick_box(&mut self, pos: (usize, usize), direction: (i8, i8)) {
        if direction.0 != 0 {
            // Horizontal
            let mut new_pos = pos;
            let mut last_box: Tile = self.map[new_pos.1][new_pos.0].clone();
            while self.map[new_pos.1][new_pos.0] == Tile::BoxLeft || self.map[new_pos.1][new_pos.0] == Tile::BoxRight {
                last_box = self.map[new_pos.1][new_pos.0].clone();
                new_pos.0 = (new_pos.0 as i8 + direction.0) as usize;
            }
            if self.map[new_pos.1][new_pos.0] == Tile::Empty {
                self.map[pos.1][pos.0] = Tile::Empty;
                while pos.0 != new_pos.0 {
                    self.map[new_pos.1][new_pos.0] = last_box.clone();
                    new_pos.0 = (new_pos.0 as i8 - direction.0) as usize;
                    if last_box == Tile::BoxLeft {
                        last_box = Tile::BoxRight;
                    } else {
                        last_box = Tile::BoxLeft;
                    }
                }
            }
        } else {
            // Vertical
            let mut new_y = pos.1;
            let mut new_x: HashSet<usize> = HashSet::from([pos.0]);
            let mut pushing_boxes: Vec<(usize, usize)> = Vec::new(); // box origins on left side
            if self.map[pos.1][pos.0] == Tile::BoxLeft {
                new_x.insert(pos.0 + 1);
                pushing_boxes.push((pos.0, pos.1));
            } else {
                new_x.insert(pos.0 - 1);
                pushing_boxes.push((pos.0 - 1, pos.1));
            }
            let mut can_push = true;
            'can_push: while new_x.len() > 0 {
                new_y = (new_y as i8 + direction.1) as usize;
                let mut next_x: HashSet<usize> = HashSet::new();
                for i in 0..new_x.len() {
                    let x = new_x.iter().nth(i).unwrap();
                    let new_pos = (*x, new_y);
                    if self.map[new_pos.1][new_pos.0] == Tile::Wall {
                        can_push = false;
                        break 'can_push;
                    } else if self.map[new_pos.1][new_pos.0] == Tile::BoxLeft {
                        next_x.insert(new_pos.0);
                        next_x.insert(new_pos.0 + 1);
                        pushing_boxes.push((new_pos.0, new_pos.1));
                    } else if self.map[new_pos.1][new_pos.0] == Tile::BoxRight {
                        next_x.insert(new_pos.0 - 1);
                        next_x.insert(new_pos.0);
                        pushing_boxes.push((new_pos.0 - 1, new_pos.1));
                    }
                }
                new_x = next_x;
            }
            if can_push {
                for b in &pushing_boxes {
                    self.map[b.1][b.0] = Tile::Empty;
                    self.map[b.1][b.0 + 1] = Tile::Empty;
                }
                for b in &pushing_boxes {
                    self.map[b.1 + direction.1 as usize][b.0] = Tile::BoxLeft;
                    self.map[b.1 + direction.1 as usize][b.0 + 1] = Tile::BoxRight;
                }
            }
        }
    }

    fn gps_sum(self) -> u64 {
        let mut sum = 0;
        for y in 0..self.map.len() {
            for x in 0..self.map[0].len() {
                if self.map[y][x] == Tile::Box || self.map[y][x] == Tile::BoxLeft {
                    sum += (y * 100 + x) as u64;
                }
            }
        }
        sum
    }

    fn print(&self) {
        let mut output: String = "".to_string();
        for y in 0..self.map.len() {
            for x in 0..self.map[0].len() {
                if self.robot_position.0 == x && self.robot_position.1 == y {
                    output.push_str("@");
                } else {
                    match self.map[y][x] {
                        Tile::Box => output.push_str("O"),
                        Tile::Empty => output.push_str("."),
                        Tile::Wall => output.push_str("#"),
                        Tile::BoxLeft => output.push_str("["),
                        Tile::BoxRight => output.push_str("]"),
                    }
                }
            }
            output.push_str("\n");
        }
        println!("{}", output);
    }
}

fn parse_map(data: &str) -> Warehouse {
    let mut map: Vec<Vec<Tile>> = Vec::new();
    let mut robot_position = (0, 0);
    let mut y = 0;
    for line in data.lines() {
        let mut row: Vec<Tile> = Vec::new();
        let mut x = 0;
        for c in line.chars() {
            let mut tile = Tile::Empty;
            match c {
                'O' => tile = Tile::Box,
                '#' => tile = Tile::Wall,
                '@' => robot_position = (x, y),
                _ => ()
            }
            row.push(tile);
            x += 1;
        }
        map.push(row);
        y += 1;
    }
    Warehouse {map, robot_position}
}

fn parse_map_thicc(data: &str) -> Warehouse {
    let mut map: Vec<Vec<Tile>> = Vec::new();
    let mut robot_position = (0, 0);
    let mut y = 0;
    for line in data.lines() {
        let mut row: Vec<Tile> = Vec::new();
        let mut x = 0;
        for c in line.chars() {
            let mut tile = Tile::Empty;
            match c {
                'O' => tile = Tile::Box,
                '#' => tile = Tile::Wall,
                '@' => robot_position = (x, y),
                _ => ()
            }
            if tile == Tile::Box {
                row.push(Tile::BoxLeft);
                row.push(Tile::BoxRight);
            } else {
                row.push(tile.clone());
                row.push(tile);
            }
            x += 2;
        }
        map.push(row);
        y += 1;
    }
    Warehouse {map, robot_position}
}

fn parse_input(path: &str, thick: bool) -> (Warehouse, String) {
    let data = fs::read_to_string(path).unwrap();
    let data = data.trim().replace("\r", "");
    let parts: Vec<&str> = data.split("\n\n").collect();

    let warehouse = if thick { parse_map_thicc(parts[0]) } else { parse_map(parts[0]) };
    let instructions = parts[1].to_string();

    (warehouse, instructions)
}

fn part1(path: &str) -> u64 {
    let (mut warehouse, instructions) = parse_input(path, false);
    execute_instructions(&mut warehouse, &instructions);
    warehouse.print();
    warehouse.gps_sum()
}

fn execute_instructions(warehouse: &mut Warehouse, instructions: &str) {
    for instruction in instructions.chars() {
        match instruction {
            '^' => warehouse.move_robot((0, -1)),
            '>' => warehouse.move_robot((1, 0)),
            'v' => warehouse.move_robot((0, 1)),
            '<' => warehouse.move_robot((-1, 0)),
            _ => ()
        }
    }
}

fn part2(path: &str) -> u64 {
    let (mut warehouse, instructions) = parse_input(path, true);
    warehouse.print();
    execute_instructions(&mut warehouse, &instructions);
    warehouse.print();
    warehouse.gps_sum()
}
