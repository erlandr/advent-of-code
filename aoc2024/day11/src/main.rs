use std::collections::HashMap;
use std::fs;
use std::mem;
use std::time::Instant;

fn main() {
    let start = Instant::now();

    let example = parse("data/example.txt");
    let input = parse("data/input.txt");

    println!("Advent of Code 2024 - Day 11");
    println!();
    println!("Example Part 1: {}", part1(&example));
    println!("Example Part 2: {}", part2(&example));
    println!();
    println!("Part 1: {}", part1(&input));
    println!("Part 2: {}", part2(&input));
    println!();

    let duration = start.elapsed();
    println!("Time elapsed: {:?}", duration);
}

fn parse(data_path: &str) -> Vec<u64> {
    let data = fs::read_to_string(data_path).unwrap();
    let stones = data
        .split_whitespace()
        .map(|x| x.parse().unwrap())
        .collect();
    return stones;
}

fn get_digits(n: u64) -> u64 {
    let mut digits = 0;
    let mut n = n;
    while n > 0 {
        digits += 1;
        n /= 10;
    }
    digits
}

fn apply_rules(stone: u64, count: u64, stone_counts: &mut HashMap<u64, u64>) -> () {
    // 0 becomes 1
    if stone == 0 {
        *stone_counts.entry(1).or_insert(0) += count;
    } else {
        // if stone is an even number of digits, split into two separate stones
        let digits = get_digits(stone);
        if digits % 2 == 0 {
            let half = 10u64.pow((digits / 2) as u32);
            let first = stone / half;
            let second = stone % half;
            *stone_counts.entry(first).or_insert(0) += count;
            *stone_counts.entry(second).or_insert(0) += count;
        } else {
            // otherwise multiply by 2024
            *stone_counts.entry(stone * 2024).or_insert(0) += count;
        }
    }
}

fn solve(stones: &Vec<u64>, n: u64) -> u64 {
    // keep track of values and number of occurrences for each value in a hashmap
    let mut stone_counts = HashMap::new();
    for &stone in stones {
        let count = stone_counts.entry(stone).or_insert(0);
        *count += 1;
    }
    // apply rules using the hashmap keys
    let mut new_stone_counts = HashMap::new();
    for _ in 0..n {
        new_stone_counts.clear();
        for (&value, &count) in stone_counts.iter() {
            apply_rules(value, count, &mut new_stone_counts);
        }
        mem::swap(&mut stone_counts, &mut new_stone_counts);
    }
    // count the values in the hashmap
    stone_counts.values().sum()
}

fn part1(stones: &Vec<u64>) -> u64 {
    solve(stones, 25)
}

fn part2(stones: &Vec<u64>) -> u64 {
    solve(stones, 75)
}
