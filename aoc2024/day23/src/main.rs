use std::fs;
use std::time::Instant;
use std::collections::{HashMap, HashSet};
use petgraph::graph::{NodeIndex, UnGraph};

fn main() {
    let start = Instant::now();

    println!("Advent of Code 2024 - Day 23");
    println!();
    println!("Example Part 1: {}", part1("data/example.txt"));
    println!("Example Part 2: {}", part2("data/example.txt"));
    println!();
    println!("Part 1: {}", part1("data/input.txt"));
    println!("Part 2: {}", part2("data/input.txt"));
    println!();

    let duration = start.elapsed();
    println!("Time elapsed: {:?}", duration);
}

fn parse(path: &str) -> Vec<(String, String)> {
    fs::read_to_string(path)
        .unwrap()
        .lines()
        .map(|line: &str| {
            let (part1, part2) = line.split_once("-").unwrap();
            (part1.to_string(), part2.to_string())
        })
        .collect()
}

fn create_graph(pairs: Vec<(String, String)>) -> UnGraph<String, ()> {
    let mut graph = UnGraph::<String, ()>::new_undirected();
    let mut node_indices: HashMap<String, NodeIndex> = HashMap::new();

    for (part1, part2) in pairs {
        let node1 = *node_indices.entry(part1.clone()).or_insert_with(|| {
            let index = graph.add_node(part1.clone());
            index
        });
        let node2 = *node_indices.entry(part2.clone()).or_insert_with(|| {
            let index = graph.add_node(part2.clone());
            index
        });
        graph.add_edge(node1, node2, ());
    }

    graph
}

fn find_triangles(graph: &UnGraph<String, ()>) -> Vec<(String, String, String)> {
    let mut triangles = HashSet::new();

    for node in graph.node_indices() {
        let neighbors: Vec<_> = graph.neighbors(node).collect();
        for i in 0..neighbors.len() {
            for j in i + 1..neighbors.len() {
                if graph.contains_edge(neighbors[i], neighbors[j]) {
                    let mut triangle = vec![
                        graph[node].clone(),
                        graph[neighbors[i]].clone(),
                        graph[neighbors[j]].clone(),
                    ];
                    triangle.sort();
                    triangles.insert((
                        triangle[0].clone(),
                        triangle[1].clone(),
                        triangle[2].clone(),
                    ));
                }
            }
        }
    }

    triangles.into_iter().collect()
}

fn find_largest_group(graph: &UnGraph<String, ()>) -> Vec<String> {
    // each member must have edges with every other member
    let mut largest_group = HashSet::new();
    for node in graph.node_indices() {
        let mut group = HashSet::new();
        group.insert(graph[node].clone());
        let neighbors: Vec<_> = graph.neighbors(node).collect();
        let mut node_num_connections: HashMap<NodeIndex, usize> = HashMap::new();
        for i in 0..neighbors.len() {
            for j in i + 1..neighbors.len() {
                if graph.contains_edge(neighbors[i], neighbors[j]) {
                    node_num_connections.entry(neighbors[i]).and_modify(|e| *e += 1).or_insert(1);
                    node_num_connections.entry(neighbors[j]).and_modify(|e| *e += 1).or_insert(1);
                }
            }
        }
        let max_connections = *node_num_connections.values().max().unwrap();
        if max_connections >= 2 {
            for (node, num_connections) in node_num_connections {
                if num_connections == max_connections {
                    group.insert(graph[node].clone());
                }
            }
            if group.len() > largest_group.len() {
                largest_group = group.clone();
            }
        }
    }
    largest_group.into_iter().collect()
}

fn part1(path: &str) -> isize {
    let mut sum = 0;
    let pairs = parse(path);
    let graph = create_graph(pairs);
    let triangles = find_triangles(&graph);
    for triangle in triangles {
        if triangle.0.starts_with("t") || triangle.1.starts_with("t") || triangle.2.starts_with("t")
        {
            sum += 1;
        }
    }
    sum
}

fn part2(path: &str) -> String {
    let pairs = parse(path);
    let graph = create_graph(pairs);
    let mut largest_group = find_largest_group(&graph);
    largest_group.sort();
    let mut result = String::new();
    for (i, member) in largest_group.iter().enumerate() {
        if i > 0 {
            result.push(',');
        }
        result.push_str(member);
    }
    result
}
