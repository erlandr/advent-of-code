use std::fs;
use std::time::Instant;
use std::collections::HashMap;
use std::collections::HashSet;

fn main() {
    let start = Instant::now();

    let example_data = fs::read_to_string("data/example.txt").unwrap();
    let input_data = fs::read_to_string("data/input.txt").unwrap();

    println!("Advent of Code 2024 - Day 8");
    println!();
    println!("Example Part 1: {}", part1(&example_data));
    println!("Example Part 2: {}", part2(&example_data));
    println!();
    println!("Part 1: {}", part1(&input_data));
    println!("Part 2: {}", part2(&input_data));
    println!();

    let duration = start.elapsed();
    println!("Time elapsed: {:?}", duration);
}

fn parse(data: &str) -> (i32, i32, HashMap<String, Vec<(i32, i32)>>) {
    let mut antenna_map: HashMap<String, Vec<(i32, i32)>> = HashMap::new();
    let data_lines: Vec<&str> = data.lines().collect();
    let height = data_lines.len();
    let width = data_lines[0].len();
    for y in 0..height {
        let line = data_lines[y];
        for x in 0..width {
            let c = line.chars().nth(x).unwrap();
            if c != '.' {
                let antenna = antenna_map.entry(c.to_string()).or_insert(Vec::new());
                antenna.push((x as i32, y as i32));
            }
        }
    }
    (width as i32, height as i32, antenna_map)
}

fn in_bounds(pos: (i32, i32), width: i32, height: i32) -> bool {
    pos.0 >= 0 && pos.0 < width && pos.1 >= 0 && pos.1 < height
}

fn find_antinodes(pos1: (i32, i32), pos2: (i32, i32), width: i32, height: i32, repeat: bool) -> Vec<(i32, i32)> {
    let mut antinodes: Vec<(i32, i32)> = Vec::new();
    if repeat {
        antinodes.push(pos1);
        antinodes.push(pos2);
    }
    let dx = pos2.0 - pos1.0;
    let dy = pos2.1 - pos1.1;
    // add antinodes until out of bounds
    let mut an = (pos1.0 - dx, pos1.1 - dy);
    while in_bounds(an, width, height) {
        antinodes.push(an);
        if !repeat {
            break;
        }
        an = (an.0 - dx, an.1 - dy);
    }
    an = (pos2.0 + dx, pos2.1 + dy);
    while in_bounds(an, width, height) {
        antinodes.push(an);
        if !repeat {
            break;
        }
        an = (an.0 + dx, an.1 + dy);
    }
    antinodes
}

fn solve(data: &str, repeat: bool) -> i64 {
    let (width, height, antenna_map) = parse(data);
    let mut unique_positions: HashSet<(i32, i32)> = HashSet::new();
    for (_antenna, positions) in &antenna_map {
        // find antinodes for each pair of positions
        for i in 0..positions.len() {
            for j in i+1..positions.len() {
                let antinodes = find_antinodes(positions[i], positions[j], width, height, repeat);
                // add antinodes to unique_positions
                for antinode in antinodes {
                    unique_positions.insert(antinode);
                }
            }
        }
    }
    unique_positions.len() as i64
}

fn part1(data: &str) -> i64 {
    solve(data, false)
}

fn part2(data: &str) -> i64 {
    solve(data, true)
}
