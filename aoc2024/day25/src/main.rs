use std::fs;
use std::time::Instant;

fn main() {
    let start = Instant::now();

    println!("Advent of Code 2024 - Day 25");
    println!();
    println!("Example Part 1: {}", part1("data/example.txt"));
    println!();
    println!("Part 1: {}", part1("data/input.txt"));
    println!();

    let duration = start.elapsed();
    println!("Time elapsed: {:?}", duration);
}

type Key = [usize; 5];

fn parse(path: &str) -> (Vec<Key>, Vec<Key>) {
    let text = fs::read_to_string(path).unwrap().replace("\r", "");
    let chunks = text.split("\n\n");
    let mut keys: Vec<Key> = Vec::new();
    let mut locks: Vec<Key> = Vec::new();
    for chunk in chunks {
        let mut item = [0; 5];
        let lines: Vec<&str> = chunk.lines().collect();
        for l in 1..lines.len() - 1{
            let counts: Vec<usize> = lines[l].chars().map(|c| if c == '#' { 1 } else { 0 }).collect();
            for i in 0..counts.len() {
                item[i] += counts[i];
            }
        }
        if lines[0] == "#####" {
            locks.push(item);
        } else {
            keys.push(item);
        }
    }
    (locks, keys)
}

fn part1(path: &str) -> isize {
    let mut sum = 0;
    let (locks, keys) = parse(path);
    for l in 0..locks.len() {
        'keyloop: for k in 0..keys.len() {
            for i in 0..5 {
                if locks[l][i] + keys[k][i] > 5 {
                    continue 'keyloop;
                }
            }
            sum += 1;
        }
    }
    sum
}
