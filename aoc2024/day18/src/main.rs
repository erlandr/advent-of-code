use std::collections::HashMap;
use std::fs;
use std::time::Instant;

fn main() {
    let start = Instant::now();

    // let (warehouse_example, instructions_example) = parse_input("data/example.txt");
    // let warehouse = parse_input("data/input.txt");

    println!("Advent of Code 2024 - Day 18");
    println!();
    println!("Example Part 1: {}", part1("data/example.txt", 7, 12));
    // println!("Example Part 1: {}", part2("data/example.txt", 7, 12));
    // println!("Example Part 2: {}", part2("data/input.txt"));
    println!();
    println!("Part 1: {}", part1("data/input.txt", 71, 1024));
    println!("Part 1: {}", part2("data/input.txt", 71, 1024));
    // println!("Part 2: {}", part2(&rules, &updates));
    println!();

    let duration = start.elapsed();
    println!("Time elapsed: {:?}", duration);
}

#[derive(PartialEq, Eq, Clone)]
enum Tile {
    Empty,
    Wall,
}

struct Map {
    size: usize,
    grid: Vec<Vec<Tile>>,
    bytes: Vec<(usize, usize)>,
}

impl Map {
    fn reset_grid(&mut self) {
        self.grid = Vec::new();
        for _ in 0..self.size {
            self.grid.push(vec![Tile::Empty; self.size])
        }
    }

    fn grid_at_time(&mut self, time: usize) {
        self.reset_grid();
        for t in 0..time {
            let (x, y) = self.bytes[t];
            self.grid[y][x] = Tile::Wall;
        }
    }

    fn shortest_path(&mut self) -> usize {
        let mut check_pos: Vec<(usize, usize)> = vec![(0, 0)];
        let mut visited: HashMap<(usize, usize), usize> = HashMap::new();
        let mut et: usize = 0;
        while check_pos.len() > 0 {
            let mut new_check_pos: Vec<(usize, usize)> = Vec::new();
            while let Some((x, y)) = check_pos.pop() {
                if self.grid[y][x] == Tile::Empty {
                    let mut space_time = et;
                    match visited.get(&(x, y)) {
                        Some(t) => space_time = *t,
                        None => space_time = et + 1,
                    }
                    if space_time > et {
                        visited.insert((x, y), et);
                        if x < self.size - 1 {
                            new_check_pos.push((x + 1, y));
                        }
                        if x > 0 {
                            new_check_pos.push((x - 1, y));
                        }
                        if y < self.size - 1 {
                            new_check_pos.push((x, y + 1));
                        }
                        if y > 0 {
                            new_check_pos.push((x, y - 1));
                        }
                    }
                }
            }
            check_pos = new_check_pos;
            et += 1;
        }
        visited[&(self.size - 1, self.size - 1)]
    }
}

fn parse_map(path: &str, size: usize) -> Map {
    let bytes = fs::read_to_string(path)
        .unwrap()
        .lines()
        .map(|line: &str| {
            let (x, y) = line.split_once(",").unwrap();
            (x.parse().unwrap(), y.parse().unwrap())
        })
        .collect();
    let grid: Vec<Vec<Tile>> = Vec::new();
    Map { size, grid, bytes }
}

fn part1(path: &str, size: usize, time: usize) -> usize {
    let mut map = parse_map(path, size);
    map.grid_at_time(time);
    map.shortest_path()
}

fn part2(path: &str, size: usize, time: usize) -> usize {
    let mut map = parse_map(path, size);
    for t in time..map.bytes.len() {
        map.grid_at_time(t);
        let (x, y) = map.bytes[t];
        let shortest = map.shortest_path();
        println!("{} ns = byte {} {}, shortest {}", t, x, y, shortest);

    }
    0
}
