use std::fs;
use std::time::Instant;

fn main() {
    let start = Instant::now();

    println!("Advent of Code 2024 - Day 20");
    println!();
    println!("Example Part 1: {}", solve("data/example.txt", 1, 2));
    println!("Example Part 2: {}", solve("data/example.txt", 50, 20));
    println!();
    println!("Part 1: {}", solve("data/input.txt", 100, 2));
    println!("Part 2: {}", solve("data/input.txt", 100, 20));
    println!();

    let duration = start.elapsed();
    println!("Time elapsed: {:?}", duration);
}

#[derive(PartialEq, Eq, Clone, Debug)]
enum Tile {
    Empty,
    Wall,
    Start,
    End,
}

struct Map {
    grid: Vec<Vec<Tile>>,
    start: (usize, usize),
    end: (usize, usize),
}

impl Map {
    fn find_path(&self) -> Vec<(usize, usize)> {
        let mut path = vec![self.start];
        let mut pos = self.start;
        let mut prev = (usize::MAX, usize::MAX);
        while pos != self.end {
            for dir in vec![(0, -1), (0, 1), (-1, 0), (1, 0)] {
                let x = (pos.0 as i32 + dir.0) as usize;
                let y = (pos.1 as i32 + dir.1) as usize;
                if (x, y) != prev && self.grid[y as usize][x as usize] != Tile::Wall {
                    prev = pos;
                    pos = (x, y);
                    path.push(pos);
                    break;
                }
            }
        }
        path
    }

    fn cheat_paths(&self, threshold: usize, cheat_time: usize) -> usize {
        let mut result = 0;
        let path = self.find_path();
        for i in 0..(path.len() - 1) {
            for j in (i + 1)..path.len() {
                let manhattan_dist = (path[j].0 as i32 - path[i].0 as i32).abs() as usize
                + (path[j].1 as i32 - path[i].1 as i32).abs() as usize;
                if manhattan_dist > cheat_time {
                    continue;
                }
                let path_dist = j - i;
                if path_dist > manhattan_dist && path_dist - manhattan_dist >= threshold {
                    result += 1;
                }
            }
        }
        result
    }
}

fn parse_map(path: &str) -> Map {
    let mut start = (0, 0);
    let mut end = (0, 0);
    let mut grid: Vec<Vec<Tile>> = Vec::new();

    for (y, line) in fs::read_to_string(path).unwrap().lines().enumerate() {
        let row: Vec<Tile> = line
            .chars()
            .enumerate()
            .map(|(x, c)| match c {
                '#' => Tile::Wall,
                '.' => Tile::Empty,
                'S' => {
                    start = (x, y);
                    Tile::Start
                }
                'E' => {
                    end = (x, y);
                    Tile::End
                }
                _ => panic!("Invalid character in map"),
            })
            .collect();
        grid.push(row);
    }

    Map { grid, start, end }
}

fn solve(path: &str, theshold: usize, cheat_time: usize) -> usize {
    let map = parse_map(path);
    map.cheat_paths(theshold, cheat_time)
}
