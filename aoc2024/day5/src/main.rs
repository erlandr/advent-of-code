use std::fs;
use std::time::Instant;
use std::collections::HashMap;

fn main() {
    let start = Instant::now();

    let (ex_rules, ex_updates) = parse_input("data/example.txt");
    let (rules, updates) = parse_input("data/input.txt");

    println!("Advent of Code 2024 - Day 5");
    println!();
    println!("Example Part 1: {}", part1(&ex_rules, &ex_updates));
    println!("Example Part 2: {}", part2(&ex_rules, &ex_updates));
    println!();
    println!("Part 1: {}", part1(&rules, &updates));
    println!("Part 2: {}", part2(&rules, &updates));
    println!();

    let duration = start.elapsed();
    println!("Time elapsed: {:?}", duration);
}

fn parse_input(path: &str) -> (HashMap<i32,Vec<i32>>,Vec<Vec<i32>>) {
    let data = fs::read_to_string(path).unwrap();
    let data = data.replace("\r", "");
    let parts: Vec<&str> = data.split("\n\n").collect();

    let mut rules: HashMap<i32,Vec<i32>> = HashMap::new();
    for line in parts[0].lines() {
        let halves: Vec<&str> = line.split("|").collect();
        let key = halves[0].parse::<i32>().unwrap();
        let value: i32 = halves[1].parse::<i32>().unwrap();
        if !rules.contains_key(&key) {
            rules.insert(key, Vec::new());
        }
        rules.get_mut(&key).unwrap().push(value);
    }

    let updates: Vec<Vec<i32>> = parts[1].lines().map(|line| {
        line.split(",").map(|num| num.parse::<i32>().unwrap()).collect()
    }).collect();

    (rules, updates)
}

fn find_middle_page(pages: &Vec<i32>) -> i32 {
    let index: usize = pages.len() / 2;
    pages[index]
}

fn part1(rules: &HashMap<i32, Vec<i32>>, updates: &Vec<Vec<i32>>) -> i32 {
    let mut sum: i32 = 0;
    for pages in updates {
        let mut valid = true;
        let mut prev_pages: Vec<i32> = Vec::new();
        'page_loop: for page in pages {
            if rules.contains_key(page) {
                for page_must_come_after in rules.get(page).unwrap() {
                    if prev_pages.contains(page_must_come_after) {
                        valid = false;
                        break 'page_loop;
                    }
                }
            }
            prev_pages.push(*page);
        }
        if valid {
            sum += find_middle_page(pages);
        }
    }
    sum
}

fn part2(rules: &HashMap<i32, Vec<i32>>, updates: &Vec<Vec<i32>>) -> i32 {
    let mut sum: i32 = 0;
    let mut updates = updates.clone();
    for pages in updates.iter_mut() {
        let mut corrected: bool = false;
        let mut i = 0;
        while i < pages.len() {
            let page = pages[i];
            let prev_pages = pages[0..i].to_vec();
            if rules.contains_key(&page) {
                for page_must_come_after in rules.get(&page).unwrap() {
                    if prev_pages.contains(page_must_come_after) {
                        // move page_must_come_after to the end of pages
                        let index = pages.iter().position(|&x| x == *page_must_come_after).unwrap();
                        pages.remove(index);
                        pages.push(*page_must_come_after);
                        corrected = true;
                        i -= 1;
                    }
                }
            }
            i += 1;
        }
        if corrected {
            sum += find_middle_page(&pages);
        }
    }
    sum
}
