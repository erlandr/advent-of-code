use std::fs;
use std::time::Instant;

fn main() {
    let start = Instant::now();

    let example_disk_map = parse("data/example.txt");
    let mut example_blocks = file_blocks(&example_disk_map);

    let input_disk_map = parse("data/input.txt");
    let mut input_blocks = file_blocks(&input_disk_map);

    println!("Advent of Code 2024 - Day 9");
    println!();
    println!("Example Part 1: {}", part1(&example_blocks));
    println!("Example Part 2: {}", part2(&example_disk_map, &mut example_blocks));
    println!();
    println!("Part 1: {}", part1(&input_blocks));
    println!("Part 2: {}", part2(&input_disk_map, &mut input_blocks));
    println!();

    let duration = start.elapsed();
    println!("Time elapsed: {:?}", duration);
}

fn parse(data_path: &str) -> Vec<i32> {
    let data = fs::read_to_string(data_path).unwrap();
    let data: Vec<i32> = data.trim().chars().map(|x| x.to_digit(10).unwrap() as i32).collect();
    return data;
}

fn file_blocks(disk_map: &Vec<i32>) -> Vec<i32> {
    // convert disk map format (<file len><free space>..) into blocks of data
    let mut blocks: Vec<i32> = Vec::new();
    for i in 0..disk_map.len() {
        let val: i32 = disk_map[i];
        if i % 2 == 0 {
            let index: i32 = (i / 2) as i32;
            for _ in 0..val {
                blocks.push(index);
            }
        } else {
            for _ in 0..val {
                blocks.push(-1);
            }
        }
    }
    blocks
}

fn defrag_partial(file_blocks: &Vec<i32>) -> u64 {
    // fill empty space "-1" by moving file blocks from the end of the string
    let mut checksum = 0;
    let mut j = file_blocks.len(); // start one past the end of the string so we can decrement
    for i in 0..file_blocks.len() {
        if j <= i {
            break;
        }
        let mut c = file_blocks[i];
        while c == -1 {
            j -= 1;
            c = file_blocks[j];
        }
        checksum += c as u64 * i as u64;
    }
    checksum
}

fn defrag_whole(disk_map: &Vec<i32>, file_blocks: &mut Vec<i32>) -> u64 {
    // keep whole files together rather than splitting them up
    let mut disk_map_defrag = disk_map.clone();
    // Get current file locations
    let mut file_locations: Vec<usize> = Vec::new();
    let mut block_index: usize = 0;
    for i in 0..disk_map_defrag.len() {
        if i % 2 == 0 {
            file_locations.push(block_index);
        }
        block_index += disk_map_defrag[i] as usize;
    }
    // Move files to the first free space
    for j in (0..disk_map_defrag.len()).rev() {
        if j % 2 == 0 { // pay attention to files, not empty space
            block_index = 0;
            let size = disk_map[j];
            let file_index = (j / 2) as i32;
            for i in 0..j {
                if i % 2 == 1 && disk_map_defrag[i] >= size { // make sure i is an empty space and it is big enough
                    // move the file
                    for k in 0..size as usize {
                        // remove the file from its current location
                        file_blocks[file_locations[file_index as usize] + k] = -1;
                        // write the file to the first free space                
                        file_blocks[block_index + k] = file_index;
                    }
                    disk_map_defrag[i-1] += size;
                    disk_map_defrag[i] -= size;
                    break;
                }
                block_index += disk_map_defrag[i] as usize;
            }
        }
    }
    let mut checksum = 0;
    for i in 0..file_blocks.len() {
        let c = file_blocks[i];
        if c == -1 {
            continue;
        }
        checksum += c as u64 * i as u64;
    }
    checksum
}

fn part1(blocks: &Vec<i32>) -> u64 {
    let checksum = defrag_partial(blocks);
    checksum
}

fn part2(disk_map: &Vec<i32>, blocks: &mut Vec<i32>) -> u64 {
    let checksum = defrag_whole(disk_map, blocks);
    checksum
}
