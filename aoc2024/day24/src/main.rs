use std::collections::HashMap;
use std::fs;
use std::time::Instant;

fn main() {
    let start = Instant::now();

    println!("Advent of Code 2024 - Day 24");
    println!();
    println!("Example Part 1: {}", part1("data/example2.txt"));
    // println!("Example Part 2: {}", part2("data/example2.txt"));
    println!();
    println!("Part 1: {}", part1("data/input.txt"));
    println!("Part 2: {}", part2("data/input.txt"));
    println!();

    let duration = start.elapsed();
    println!("Time elapsed: {:?}", duration);
}

#[derive(Copy, Clone, Debug)]
enum Operator {
    AND,
    OR,
    XOR,
}

#[derive(Clone, Debug)]
struct Gate {
    input1: String,
    input2: String,
    operator: Operator,
    output: String,
}

struct Device {
    memory: HashMap<String, bool>,
    gates: Vec<Gate>,
}

impl Device {
    fn process(&mut self) {
        while self.gates.len() > 0 {
            let mut gates_to_remove: Vec<usize> = Vec::new();
            for i in 0..self.gates.len() {
                if let Some(inp1) = self.memory.get(&self.gates[i].input1) {
                    if let Some(inp2) = self.memory.get(&self.gates[i].input2) {
                        let out = match self.gates[i].operator {
                            Operator::AND => inp1 & inp2,
                            Operator::OR => inp1 | inp2,
                            Operator::XOR => inp1 ^ inp2,
                        };
                        self.memory.insert(self.gates[i].output.clone(), out);
                        // self.gates.remove(i);
                        gates_to_remove.push(i);
                    }
                }
            }
            gates_to_remove.sort();
            gates_to_remove.reverse();
            for i in gates_to_remove {
                self.gates.remove(i);
            }
        }
    }

    fn addends(&self) -> (usize, String, usize, String) {
        let mut x = 0;
        let mut y = 0;
        let mut x_bin = String::new();
        let mut y_bin = String::new();
        for i in (0..46).rev() {
            let x_address = &("x".to_owned() + &format!("{:0>2}", &i.to_string()));
            if self.memory.get(x_address) != None {
                if self.memory[x_address] { 
                    x_bin += "1";
                    x += 1 << i;
                } else { 
                    x_bin += "0";
                }
                let y_address = &("y".to_owned() + &format!("{:0>2}", &i.to_string()));
                if self.memory[y_address] { 
                    y_bin += "1";
                    y += 1 << i;
                } else { 
                    y_bin += "0";
                }
            } else {
                x_bin = "0".to_string() + &x_bin;
                y_bin = "0".to_string() + &y_bin;
            }
        }
        (x, x_bin, y, y_bin)
    }

    fn answer(&self) -> usize {
        let mut sum = 0;
        for i in 0..1000 {
            let address = &("z".to_owned() + &format!("{:0>2}", &i.to_string()));
            if let Some(o) = self.memory.get(address) {
                if *o {
                    sum += 1 << i;
                }
            } else {
                break;
            }
        }
        sum
    }

    fn debug(&self) {
        let mut bins: Vec<Vec<Gate>> = Vec::new();
        for _ in 0..45 {
            bins.push(Vec::new());
        }
        for i in 0..self.gates.len() {
            let gate = &self.gates[i];
            match gate.operator {
                Operator::XOR => {
                    if gate.input1.starts_with("x") || gate.input1.starts_with("y") {
                        let num: usize = gate.input1[1..].parse().unwrap();
                        bins[num].push(gate.clone());
                    } else if gate.output.starts_with("z") {
                        let num: usize = gate.output[1..].parse().unwrap();
                        bins[num].push(gate.clone());

                    } else {
                        println!("Problem with gate - {:?}", gate);
                    }
                },
                Operator::AND => {

                },
                Operator::OR => {

                }
            }
        }
        for b in bins.iter() {
            print!("{} ", b.len().to_string());
        }
    }
}

fn parse(path: &str) -> Device {
    let input = fs::read_to_string(path).unwrap().replace("\r", "");
    let (init_memory, gates) = input.split_once("\n\n").unwrap();
    let mut memory: HashMap<String, bool> = HashMap::new();
    for l in init_memory.lines() {
        let (k, v) = l.split_once(": ").unwrap();
        let v: bool = v == "1";
        memory.insert(k.to_string(), v);
    }
    let gates = gates
        .lines()
        .map(|g| {
            let chunks: Vec<&str> = g.split(" ").collect();
            let operator = match chunks[1] {
                "XOR" => Operator::XOR,
                "OR" => Operator::OR,
                "AND" => Operator::AND,
                _ => panic!("Unrecognized Operator!"),
            };
            Gate {
                input1: chunks[0].to_string(),
                input2: chunks[2].to_string(),
                operator: operator,
                output: chunks[4].to_string(),
            }
        })
        .collect();
    Device { memory, gates }
}

fn part1(path: &str) -> usize {
    let mut device = parse(path);
    device.process();
    device.answer()
}

fn part2(path: &str) -> usize {
    let mut device = parse(path);
    let (x, x_bin, y, y_bin) = device.addends();
    device.process();
    let answer = device.answer();
    let answer_bin= to_bin(answer);
    println!("{} {}", x_bin, x);
    println!("{} {}", y_bin, y);
    println!();
    println!("{} {}", to_bin(x + y), x + y);
    println!("{} {}", answer_bin, answer);

    let device = parse(path);
    device.debug();
    0
}

fn to_bin(num: usize) -> String {
    (0..46).rev().map (|n| ((num >> n) & 1).to_string()).collect()
}
