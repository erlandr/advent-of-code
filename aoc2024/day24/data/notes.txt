// 0
x00 XOR y00 -> z00 <
y00 AND x00 -> whb carry

// 1
x01 XOR y01 -> jjd this1
x01 AND y01 -> bdf carry1
jjd XOR whb -> z01 <
jjd AND whb -> wbw carry2
bdf OR wbw -> qkf  carry3

// 2
x02 XOR y02 -> wsv
x02 AND y02 -> vdf
qkf XOR wsv -> z02 <
qkf AND wsv -> pqc
pqc OR vdf -> bkh



x06 AND y06 -> ssv
gtn AND qmt -> jmk
jmk OR ssv -> rkv

// 7
x07 XOR y07 -> cds
x07 AND y07 -> sdj
cds XOR rkv -> rts < --
cds AND rkv -> nph
sdj OR nph -> z07 --

(rts, z07)

//8
x08 XOR y08 -> hmv
x08 AND y08 -> jrr
hmv XOR rts -> z08 <
rts AND hmv -> ptf
ptf OR jrr -> sqm

// 12
x12 XOR y12 -> nft
y12 AND x12 -> mqn
ksn XOR nft -> jpj < --
ksn AND nft -> z12 --

(jpj, z12)

//34
x34 XOR y34 -> vvw --
y34 AND x34 -> chv --
chv XOR fqf -> z34 <
fqf AND chv -> cwh

(vvw, chv)


//25
x25 XOR y25 -> knm
y25 AND x25 -> kfv
rbd XOR knm -> z25
rbd AND knm -> rdj
rdj OR kfv -> gdb

// 26
x26 XOR y26 -> bvp
x26 AND y26 -> z26
bvp XOR gdb -> kgj
bvp AND gdb -> stc
kgj OR stc -> www

(kgj, z26)


(rts, z07)(jpj, z12)(vvw, chv)(kgj, z26)

chv,jpj,kgj,rts,vvw,z07,z12,z26